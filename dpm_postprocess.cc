/*  Copyright (C) 2007 University of Oxford  */

/* S. Jbabdi */

/*  CCOPYRIGHT  */

#include <cmath>
#include <stdlib.h>

#include "utils/options.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"

using namespace Utilities;
using namespace NEWIMAGE;
using namespace NEWMAT;

string title="dpm_postprocess\n create clustering mask results";
string examples="dpm_postprocess --clusters=<out.z> --coord=<out.coord> --mask=<volume> --out=<volume>";

Option<string> clustfile(string("-k,--clusters"),"",
		       string("cluster values"),
		       true,requires_argument);
Option<string> coordfile(string("-c,--coord"),"",
		       string("coordinate file"),
		       true,requires_argument);
Option<string> maskfile(string("-m,--mask"),"",
		       string("mask file for output space"),
		       true,requires_argument);
Option<string> output(string("-o,--out"),"",
		       string("Ouput file name"),
		       true,requires_argument);


int postprocess(){
  volume<int>   omask;
  read_volume(omask,maskfile.value());  

  Matrix coord,clust;
  coord=read_ascii_matrix(coordfile.value());
  clust=read_ascii_matrix(clustfile.value());

  if(coord.Nrows() != clust.Nrows()){
    cerr << "incompatible dimensions between coordinate file and cluster file" << endl;
    exit(-1);
  }

  int minclust=clust.Minimum();

  omask=0;
  for(int i=1;i<=coord.Nrows();i++){
    omask(coord(i,1),coord(i,2),coord(i,3)) = int(clust(i,1)) - minclust + 1;
  }
  

  save_volume(omask,output.value());


  return 0;
}


int main (int argc, char *argv[]){

  Tracer tr("main");
  OptionParser options(title,examples);
  
  try{
    options.add(clustfile);
    options.add(coordfile);
    options.add(maskfile);
    options.add(output);

    options.parse_command_line(argc,argv);

    if ( !options.check_compulsory_arguments(true) ){
      options.usage();
      exit(EXIT_FAILURE);
    }
  }
  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } 
  catch(std::exception &e) {
    cerr << e.what() << endl;
  } 

  return postprocess();

}
