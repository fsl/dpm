#include <stdio.h>
#include <ostream>

#include "armawrap/newmat.h"

#include "dpm_utils.h"

using namespace std;
using namespace NEWRAN;
using namespace NEWMAT;
using namespace MISCMATHS;


namespace DPM {

std::ostream& operator << (ostream& o,GaussianWishart& g){
  g.print(o);
  return o;
}
std::ostream& operator << (ostream& o,MDGamma& g){
  g.print(o);
  return o;
}

ReturnMatrix wishrnd(const SymmetricMatrix& sigma,const int dof){
  // compute cholesky factor for sigma
  LowerTriangularMatrix L = Cholesky(sigma);
  // for small degrees of freedom, use the definition
  int n = sigma.Nrows();
  Matrix X;
  if(dof <= 81+n ){
    X.ReSize(dof,n);
    X = normrnd(dof,n) * L.t();
  }
  // otherwise, use Smith & Hocking procedure
  else{
    X.ReSize(n,n);
    Matrix A(n,n);
    for(int i=1;i<=n;i++){
      Gamma G((dof-i+1)/2);
      G.Set(rand()/float(RAND_MAX));
      for(int j=1;j<=n;j++){
	if     (i>j) { A(i,j) = 0; }
	else if(i<j) { A(i,j) = normrnd(1,1).AsScalar(); }
	else         { A(i,j) = std::sqrt(2*G.Next()); }
      }
    }
    X = A * L.t();
  }

  SymmetricMatrix res(n);
  res << X.t() * X;


  res.Release();
  return res;
}
ReturnMatrix iwishrnd(const SymmetricMatrix& sigma,const int dof){
  // assumes inv-Wishart(sigma.i(),dof)

  SymmetricMatrix res;
  res = wishrnd(sigma,dof);
  res = res.i();

  res.Release();
  return res;
}


bool compare(const pair<float,int> &r1,const pair<float,int> &r2){
  return (r1.first<r2.first);
}

void randomise(vector< pair<float,int> >& r){
  for(unsigned int i=0;i<r.size();i++){
    pair<float,int> p(rand()/float(RAND_MAX),i);
    r[i]=p;
  }
  sort(r.begin(),r.end(),compare);

}
vector< pair<float,int> > randomise(const int n){
  vector< pair<float,int> > v(n);
  randomise(v);
  return v;
}

float gamrnd(const float& a,const float& b){
  Gamma g(a);
  float ret;

  ret = g.Next();
  return ret/b;
}
float gampdf(const float& x,const float& a,const float& b){
  float res=0;

  if((a/b>0)&&(a/(b*b)>0.00001)){
    float c = lgam(a);
    if(std::abs(c) < 150){

      if(x>0.000001){
	res = std::exp(a*std::log(b) +
			   (a-1) * std::log(x)
			   - b*x - c);
      }

    }
  }

  return res;
}
float distfisher(const float& mu1,const float& mu2,const float& sig1,const float& sig2){

  float x=std::sqrt((mu1-mu2)*(mu1-mu2)+(sig1+sig2)*(sig1+sig2));
  float y=std::sqrt((mu1-mu2)*(mu1-mu2)+(sig1-sig2)*(sig1-sig2));

  return std::sqrt(2)*std::log((x+y)/(x-y));
}

float do_kmeans(const Matrix& data,vector<int>& z,int& k){
  int numiter=50;
  if(data.Nrows() != (int)z.size())
    z.resize(data.Nrows());

  int n = data.Nrows();
  int d = data.Ncols();

  Matrix means(d,k),newmeans(d,k);
  ColumnVector nmeans(k);

  means=0;
  nmeans=0;


// initialise with far-away trick
// start with a random class centre. then each new class centre is
  // as far as possible from the cog of the previous classes
  means.Column(1) = data.Row(MISCMATHS::round(rand()/float(RAND_MAX)*float(n-1))+1).t();
  ColumnVector cog(d);
  for(int cl=2;cl<=k;cl++){
    cog = sum(means.SubMatrix(1,d,1,cl-1),2);

    int maxi=1;float dist=0,maxdist=0;
    for(int i=1;i<=n;i++){
      float cdist=0,mindist=-1;int minc=1;
      for(int prevcl=cl-1;prevcl>=1;prevcl--){
	cdist = (means.Column(prevcl)-data.Row(i).t()).SumSquare();
	if(mindist==-1 || cdist<=mindist){mindist=cdist;minc=prevcl;}
      }
      dist = mindist;
      if(dist>=maxdist){maxdist=dist;maxi=i;}
    }
    means.Column(cl)=data.Row(maxi).t();
  }


  // iterate
  for(int iter=0;iter<numiter;iter++){
    //OUT(iter);
    // loop over datapoints and attribute z for closest mean
    newmeans=0;
    nmeans=0;
    for(int i=1;i<=n;i++){
      float mindist=-1,dist=0;
      int mm=1;
      for(int m=1;m<=k;m++){
	dist = (means.Column(m)-data.Row(i).t()).SumSquare();
	if( mindist<0 || dist<mindist){
	  mindist=dist;
	  mm = m;
	}
      }
      z[i-1] = mm;
      newmeans.Column(mm) += data.Row(i).t();
      nmeans(mm) += 1;
    }

    // compute means
    for(int m=1;m<=k;m++){
//       if(nmeans(m)==0){
// 	// create singleton with the point that is furthest from the old centroid
// 	float maxdist=0,dist=0;int maxi=1;
// 	for(int i=1;i<=n;i++){
// 	  dist = (means.Column(m)-data.Row(i).t()).SumSquare();
// 	  if(dist>=maxdist){maxdist=dist;maxi=i;}
// 	}
// 	newmeans.Column(m) = data.Row(maxi).t();nmeans(m)=1;
// 	z[maxi-1] = m;
//       }

      if(nmeans(m)!=0)
	newmeans.Column(m) /= nmeans(m);
    }
    means = newmeans;
  }


  return (corrcoef(means)-IdentityMatrix(k)).Maximum();

}


  void do_kmeans(const Matrix& data,vector<int>& z,int& k,const int& nrep){
    int numiter=100;
    if(data.Nrows() != (int)z.size())
      z.resize(data.Nrows());

    int n = data.Nrows();
    int d = data.Ncols();

    Matrix means(d,k),newmeans(d,k);
    ColumnVector nmeans(k);

    means=0;
    nmeans=0;

    // initialise with far-away trick
    means.Column(1) = data.Row(MISCMATHS::round(rand()/float(RAND_MAX)*float(n-1))+1).t();
    ColumnVector cog(d);
    for(int cl=2;cl<=k;cl++){
      cog = sum(means.SubMatrix(1,d,1,cl-1),2);

      int maxi=1;float dist=0,maxdist=0;
      for(int i=1;i<=n;i++){
	float cdist=0,mindist=1e20;int minc=1;
	for(int prevcl=cl-1;prevcl>=1;prevcl--){
	  cdist = (means.Column(prevcl)-data.Row(i).t()).SumSquare();
	  if(cdist<=mindist){mindist=cdist;minc=prevcl;}
	}
	dist = mindist;
	if(dist>=maxdist){maxdist=dist;maxi=i;}
      }
      means.Column(cl)=data.Row(maxi).t();
    }


    // repeats
    for(int rep=1;rep<=nrep;rep++){
      //OUT(rep);
      // iterate
      for(int iter=0;iter<numiter;iter++){
	//OUT(iter);
	// loop over datapoints and attribute z for closest mean
	newmeans=0;
	nmeans=0;
	for(int i=1;i<=n;i++){
	  float mindist=1E20,dist=0;
	  int mm=1;
	  for(int m=1;m<=k;m++){
	    dist = (means.Column(m)-data.Row(i).t()).SumSquare();
	    if( dist<mindist){
	      mindist=dist;
	      mm = m;
	    }
	  }
	  z[i-1] = mm;
	  newmeans.Column(mm) += data.Row(i).t();
	  nmeans(mm) += 1;
	}

	// compute means
	for(int m=1;m<=k;m++){
	  if(nmeans(m)==0){
	    // create singleton with the point that is furthest from the old centroid
	    float maxdist=0,dist=0;int maxi=1;
	    for(int i=1;i<=n;i++){
	      dist = (means.Column(m)-data.Row(i).t()).SumSquare();
	      if(dist>=maxdist){maxdist=dist;maxi=i;}
	    }
	    newmeans.Column(m) = data.Row(maxi).t();nmeans(m)=1;
	    z[maxi-1] = m;
	  }
	  newmeans.Column(m) /= nmeans(m);
	}
	means = newmeans;
      }// end of iter

      // change means for the next repeat

      //      OUT(means);
      int ii,jj;
      (corrcoef(means)-IdentityMatrix(k)).Maximum2(ii,jj);

      float maxdist=0,dist=0;int maxi=1;
      for(int i=1;i<=n;i++){
	dist = (means.Column(ii)-data.Row(i).t()).SumSquare();
	if(dist>=maxdist){maxdist=dist;maxi=i;}
      }
      means.Column(ii) = data.Row(maxi).t();
      //OUT(means);
    }




  }


}
