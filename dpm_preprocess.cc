/*  Copyright (C) 2007 University of Oxford  */

/* S. Jbabdi */

/*  CCOPYRIGHT  */

#include <cmath>
#include <stdlib.h>

#include "utils/options.h"
#include "newimage/newimageall.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/miscprob.h"


using namespace Utilities;
using namespace NEWIMAGE;
using namespace NEWMAT;
using namespace MISCMATHS;

string title="dpm_preprocess\n create data file for dpm clustering";
string examples="dpm_preprocess -s <seed> -t <targetfile> -o basename [--rand --reduce=10.0]";

Option<string> seed(string("-s,--seed"),"",
		       string("Seed mask volume"),
		       true,requires_argument);
Option<string> targets(string("-t,--targets"),"",
		       string("Targets file as in seed_to_target"),
		       true,requires_argument);
Option<string> directory(string("-d,--dir"),"",
		       string("probtrackx results directory"),
		       true,requires_argument);
Option<string> output(string("-o,--out"),"",
		       string("Ouput file name"),
		       true,requires_argument);
Option<bool>   addrand(string("--rand"),false,
		       string("Add random noise"),
		       false,no_argument);
Option<int>  dimreduce(string("--reduce"),0,
		       string("Number of dimensions to keep"),
		       false,requires_argument);


void read_masks(vector<string>& masks,const string& filename){
  ifstream fs(filename.c_str());
    string tmp;
    if(fs){
      fs>>tmp;
      do{
	masks.push_back(tmp);
	fs>>tmp;
      }while(!fs.eof());
    }
    else{
      cerr<<filename<<" does not exist"<<endl;
      exit(0);
    }
}
// calculate pca decomposition using the covariance method
// X is the data nxd matrix with n observations (data points) and d variables (dimensions) 
ReturnMatrix pcacov(Matrix& X,const int& dims,Matrix& projector){
  int n=X.Nrows();
  int d=X.Ncols();
  // de-mean
  cout<<"de-mean"<<endl;
  ColumnVector Xmean(d);
  for(int j=1;j<=d;j++)
    Xmean(j) = X.Column(j).Sum()/n;
  for(int j=1;j<=d;j++){
    for(int i=1;i<=n;i++)
      X(i,j) -= Xmean(j);
  }

  // calculate covariance
  cout<<"covariance"<<endl;
  SymmetricMatrix C(d);

  if(d<n)
    C << (X.t()*X)/n;
  else
    C << X*X.t()/n;

  // eigenvalues
  cout<<"eigenvalues"<<endl;
  Matrix V;
  DiagonalMatrix D;
  EigenValues(C,D,V);

  // select subset
  cout<<"variance explained:";
   float cumsum=0,total=D.Trace();
   int dim=0;
   for(int i=D.Nrows();i>=1;i--){
     cumsum += D(i);
     if(dim<=dims){dim++;}
     else{break;}
   }
   cout<<cumsum/total*100<<"%"<<endl;
//   if(dim<=2)dim=2;

//   dim=dims;

  Matrix v(V.Nrows(),dims);
  ColumnVector lam(dims);
  for(int j=1;j<=dims;j++){
    v.Column(j) = V.Column(V.Ncols()-j+1);
    lam(j)=D(V.Ncols()-j+1);
  }

  //cout<<"zscores"<<endl;
  // convert to z-scores
  //for(int i=1;i<=d;i++)
  //X.Row(i) /= sqrt(C(i,i));
  // reconstruct data
  cout<<"data"<<endl;
  Matrix data(dims,n);

  if(!(d<n)){
    v = (X.t() * v);
    for(int j=1;j<=dims;j++)
      v.Column(j) /= sqrt(n*lam(j));
  }
  data = X*v;
  
  projector.ReSize(v.Nrows(),v.Ncols());
  projector = v;

  data.Release();
  return data;
}
ReturnMatrix dimred(Matrix& data,const int& numdims){

  //Matrix projector;
  Matrix newdat,projector;
  newdat=pcacov(data,numdims,projector);
  data.ReSize(newdat.Nrows(),newdat.Ncols());
  data=newdat;

  projector.Release();
  return projector;
}

int preprocess(){
  vector<string> targetnames;
  read_masks(targetnames,targets.value());
  vector< volume<float> > targetmasks;
  volume<float> seedmask;
  read_volume(seedmask,seed.value());  

  volume<float> tmpmask;
  for(unsigned int i=0;i<targetnames.size();i++){
    read_volume(tmpmask,targetnames[i]);
    targetmasks.push_back(tmpmask);
  }

  int npoints=0;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	npoints++;
      }
  Matrix data(npoints,targetmasks.size()),coord(npoints,3);

  volume<float> lookup;
  lookup = seedmask;
  
  int curpoint=0;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	curpoint++;

	for(unsigned int i=0;i<targetmasks.size();i++){
	  float addterm=addrand.value()?0.1*normrnd(1,1).AsScalar():0.0;
	  //OUT(addterm);
	  data(curpoint,i+1) = log(targetmasks[i](x,y,z)+1)+addterm;
	}
	coord(curpoint,1) = x;
	coord(curpoint,2) = y;
	coord(curpoint,3) = z;
  
	lookup(x,y,z) = curpoint;
      }
  cout<<"data created"<<endl;
  // reduce data
  OUT(data.Ncols());
  Matrix proj;
  if(dimreduce.value()>0){
    cout << "reduce dimensionality" << endl;
    proj=dimred(data,dimreduce.value());
  }
  else{
    proj=IdentityMatrix(data.Ncols());
  }
  
  OUT(data.Ncols());
  
  // compute adjacency matrix
  Matrix adj(npoints,26);
  curpoint=0;
  for(int z=0;z<seedmask.zsize();z++)
    for(int y=0;y<seedmask.ysize();y++)
      for(int x=0;x<seedmask.xsize();x++){
	if(seedmask(x,y,z)==0)continue;
	curpoint++;
	short int curneighbour=0;
	for(int zz=-1;zz<=1;zz++)
	  for(int yy=-1;yy<=1;yy++)
	    for(int xx=-1;xx<=1;xx++){
	      if(abs(xx)+abs(yy)+abs(zz)==0)continue;
	      curneighbour++;
	      if(seedmask(x+xx,y+yy,z+zz)!=0)
		adj(curpoint,curneighbour) = lookup(x+xx,y+yy,z+zz);
	      else
		adj(curpoint,curneighbour) = -1;
	    }
	      

      }
  
  
  write_ascii_matrix(proj,output.value()+".proj");
  write_ascii_matrix(data,output.value()+".data");
  write_ascii_matrix(coord,output.value()+".coord");
  write_ascii_matrix(adj,output.value()+".adj");

  return 0;
}


int main (int argc, char *argv[]){

  Tracer tr("main");
  OptionParser options(title,examples);
  
  try{
    options.add(seed);
    options.add(targets);
    options.add(directory);
    options.add(output);
    options.add(addrand);
    options.add(dimreduce);

    options.parse_command_line(argc,argv);

    if ( !options.check_compulsory_arguments(true) ){
      options.usage();
      exit(EXIT_FAILURE);
    }
  }
  catch(X_OptionError& e) {
    options.usage();
    cerr << endl << e.what() << endl;
    exit(EXIT_FAILURE);
  } 
  catch(std::exception &e) {
    cerr << e.what() << endl;
  } 

  return preprocess();

}
