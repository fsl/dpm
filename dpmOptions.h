#if !defined(dpmOptions_h)
#define dpmOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"

namespace DPMOPTS {

class dpmOptions {
 public:
  static dpmOptions& getInstance();
  ~dpmOptions() { delete gopt; }

  Utilities::Option<bool>        help;
  Utilities::Option<bool>        verbose;

  Utilities::Option<std::string> datafile;
  Utilities::Option<std::string> logfile;
  Utilities::Option<std::string> init_class;
  Utilities::Option<std::string> likelihood;
  Utilities::Option<int>         numclass;
  Utilities::Option<bool>        hierarchical;
  Utilities::Option<bool>        infinite;
  Utilities::Option<std::string> spatial;
  Utilities::Option<float>       spcparam;

  Utilities::Option<int>         njumps;
  Utilities::Option<int>         burnin;
  Utilities::Option<int>         sampleevery;


  void parse_command_line(int argc,char** argv,Utilities::Log& logger);

 private:
  dpmOptions();
  const dpmOptions& operator=(dpmOptions&);
  dpmOptions(dpmOptions&);

  Utilities::OptionParser options;

  static dpmOptions* gopt;

};


 inline dpmOptions& dpmOptions::getInstance(){
   if(gopt == NULL)
     gopt = new dpmOptions();

   return *gopt;
 }

 inline dpmOptions::dpmOptions() :
   help(std::string("-h,--help"), false,
	std::string("display this message"),
        false,Utilities::no_argument),
   verbose(std::string("-V,--verbose"), false,
	std::string("display program outputs"),
	false,Utilities::no_argument),
   datafile(std::string("-d,--data"), std::string(""),
	    std::string("data file"),
	    true,Utilities::requires_argument),
   logfile(std::string("-o,--out"), std::string(""),
	    std::string("output basename"),
	    true, Utilities::requires_argument),
   init_class(std::string("--ic,--initclass"), "kmeans",
	    std::string("data labelling initialisation - default=kmeans"),
	    false, Utilities::requires_argument),
   likelihood(std::string("-l,--likelihood"), "GaussianWishart",
	    std::string("Likelihood model - default=GaussianWishart"),
	    false, Utilities::requires_argument),
   numclass(std::string("-k,--numclass"),5,
	    std::string("initial number of classes - default=5"),
	    false,Utilities::requires_argument),
   hierarchical(std::string("--hier"),false,
	    std::string("Hierarchical DPM - default no"),
	    false,Utilities::no_argument),
   infinite(std::string("--noinf"),true,
	    std::string("Turn off infinite mixture modelling"),
	    false,Utilities::no_argument),
   spatial(std::string("--spatial"),"",
	    std::string("Adjacency file for spatial prior"),
	    false,Utilities::requires_argument),
   spcparam(std::string("--beta"),1.0,
	    std::string("Beta parameter for spatial prior - default=1"),
	    false,Utilities::requires_argument),
   njumps(std::string("--nj,--njumps"),500,
	   std::string("number of iterations after burnin - default=500"),
	   false,Utilities::requires_argument),
   burnin(std::string("--bi,--burnin"),500,
	  std::string("number of iterations before sampling - default=500"),
	  false,Utilities::requires_argument),
   sampleevery(std::string("--se,--sampleevery"),10,
	       std::string("sampling frequency - default=10"),
	       false,Utilities::requires_argument),
   options("dpm","dpm -d data -o logfile")
     {


     try {
       options.add(help);
       options.add(verbose);
       options.add(datafile);
       options.add(logfile);
       options.add(init_class);
       options.add(likelihood);
       options.add(hierarchical);
       options.add(infinite);
       options.add(spatial);
       options.add(spcparam);
       options.add(numclass);
       options.add(njumps);
       options.add(burnin);
       options.add(sampleevery);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }

   }
}

#endif
