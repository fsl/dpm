/*  Copyright (C) 2007 University of Oxford  */

/* S. Jbabdi */

/*  CCOPYRIGHT  */

#include <stdio.h>
#include "dpm_gibbs.h"
#include "hdpm_gibbs.h"
//#include "redpm_gibbs.h"
#include "dpmOptions.h"
#include <math.h>

using namespace DPMOPTS;
using namespace DPM;
using namespace Utilities;
using namespace MISCMATHS;

int main (int argc, char *argv[]){

  Log& logger = LogSingleton::getInstance();

  dpmOptions& opts = dpmOptions::getInstance();
  opts.parse_command_line(argc,argv,logger);

  srand(time(NULL));

  // read input files
  if(!opts.hierarchical.value()){
    Matrix data; // data is nxd
    data=read_ascii_matrix(opts.datafile.value());
    if(opts.likelihood.value() == "GaussianWishart"){
      GWDPM_GibbsSampler gs(data,opts.njumps.value(),opts.burnin.value(),opts.sampleevery.value());
      gs.set_infinite(opts.infinite.value());
      if(opts.spatial.value()!="")
	gs.add_spatial_prior(opts.spatial.value());
      cout << "init" << endl;
      gs.init(opts.numclass.value(),opts.init_class.value());
      gs.set_spcparam(opts.spcparam.value());
      cout << "run" << endl;
      gs.run();
      cout << "save" << endl;
      gs.save(opts.logfile.value());
    }
    else{ 
      EXPDPM_GibbsSampler gs(data,opts.njumps.value(),opts.burnin.value(),opts.sampleevery.value());
      gs.set_infinite(opts.infinite.value());
      gs.add_spatial_prior(opts.spatial.value());

      gs.init(opts.numclass.value(),opts.init_class.value());
      gs.run();
      gs.save(opts.logfile.value());
    }
  }
  else{
    vector<Matrix> data;
    ifstream fs(opts.datafile.value().c_str());
    string tmp;
    Matrix tmpdata;
    if(fs){
      fs>>tmp;
      do{
	tmpdata=read_ascii_matrix(tmp);
	data.push_back(tmpdata);
	fs>>tmp;
      }while(!fs.eof());
    }
    else{
      cerr<<opts.datafile.value()<<" does not exist"<<endl;
      exit(0);
    }
    if(opts.likelihood.value() == "GaussianWishart"){
      GWHDPM_GibbsSampler gs(data,opts.njumps.value(),opts.burnin.value(),opts.sampleevery.value());
      gs.set_infinite(opts.infinite.value());
      gs.init(opts.numclass.value(),opts.init_class.value());
      if(opts.spatial.value()!="")
	gs.add_spatial_prior(opts.spatial.value());
      gs.set_spcparam(opts.spcparam.value());
      gs.run();
      gs.save(opts.logfile.value());
    }
    else{
      EXPHDPM_GibbsSampler gs(data,opts.njumps.value(),opts.burnin.value(),opts.sampleevery.value());
      //REDPM_GibbsSampler gs(data,opts.njumps.value(),opts.burnin.value(),opts.sampleevery.value());
      gs.init(opts.numclass.value(),opts.init_class.value());
      gs.run();
      gs.save(opts.logfile.value());
    }
    
  }
  

}

