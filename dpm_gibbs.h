#if !defined(dpm_gibbs_h)
#define dpm_gibbs_h

#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <time.h>

#include "armawrap/newmat.h"
#include "newran/newran.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/miscmaths.h"
#include "miscmaths/f2z.h"

#include "gibbs.h"
#include "dpm_utils.h"


namespace DPM {

class DPM_GibbsSampler : public GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,DPM_GibbsSampler& g);

 protected:

  // parameters            ------> estimated via gibb's sampling
  // these are common for all likelihood models
  float                    m_alpha;
  std::vector<int>              m_z;

  // hyperparameter (prior on alpha)
  float m_a0;
  float m_b0;

  // data-related quantities
  std::vector<int>         m_classnd;
  int                      m_k;
  int m_n;
  int m_d;

  // Spatial priors
  bool                     m_spat;
  NEWMAT::Matrix           m_adj; // adjacency matrix
  float                    m_logspatterm;
  float                    m_spcparam;

  std::vector< std::pair<float,int> > randindex;
  bool                                infinite;

  // dimensionality reduction
  bool m_reduced;
  NEWMAT::Matrix m_projector;


  // samples
  std::vector<float>            m_sample_alpha;
  std::vector<int>              m_sample_k;
  std::vector<double>           m_sample_likelihood;
  double                        m_likelihood;
  int                           m_nsamples;
  std::vector<float>            m_mean_z;

  NEWMAT::ColumnVector m_map_z;
  NEWMAT::Matrix m_map_fuzzy_z;

  const NEWMAT::Matrix&            m_data;

public:
  DPM_GibbsSampler(const NEWMAT::Matrix& data):m_data(data){
    m_n = m_data.Nrows();
    m_d = m_data.Ncols();

    m_nsamples = (int)floor( m_njumps / m_sampleevery );

    m_sample_alpha.resize(m_nsamples);
    m_sample_k.resize(m_nsamples);
    m_sample_likelihood.resize(m_nsamples);
    m_mean_z.resize(m_n);

    infinite = true;

    NEWRAN::Random::Set(rand() / float(RAND_MAX));
    //Random::Set(0.5);
    m_spat=false;
    infinite = true;

    m_reduced=false;
    m_projector = NEWMAT::IdentityMatrix(m_d);


  }
  DPM_GibbsSampler(const NEWMAT::Matrix& data,const int j,const int b,const int s):
  GibbsSampler(j,b,s),m_data(data){
    m_n = m_data.Nrows();
    m_d = m_data.Ncols();

    //    m_nsamples = (int)floor( m_njumps / m_sampleevery );
    m_nsamples = (int)floor( (j) / m_sampleevery );

    m_sample_alpha.resize(m_nsamples);
    m_sample_k.resize(m_nsamples);
    m_sample_likelihood.resize(m_nsamples);
    m_mean_z.resize(m_n);

    infinite = true;

    NEWRAN::Random::Set(rand() / float(RAND_MAX));
    //Random::Set(0.5);

    m_spat=false;
    m_reduced=false;
    m_projector = NEWMAT::IdentityMatrix(m_d);

 }
  ~DPM_GibbsSampler(){}


  // initialisation functions
  void init(const int& k = 5, const std::string& scheme = "kmeans");
  virtual void init_own_params() = 0;
  virtual void init_oneperdata() = 0;
  virtual void init_onebigclass() = 0;
  virtual void init_max() = 0;
  virtual void init_kmeans(const int) = 0;
  virtual void init_random(const int) = 0;

  // parent class function
  void sample_parameters();
  void sample_hyperparameters(const bool isburned=false);

  // sample model parameters
  void sample_z();
  void mode_z();
  void mode_fuzzy_z();
  void sample_alpha(const bool);

  NEWMAT::ColumnVector get_map_z()const{return m_map_z;}
  NEWMAT::Matrix get_map_fuzzy_z()const{return m_map_fuzzy_z;}

  virtual void sample_own_params() = 0;
  virtual void sample_own_hypers() = 0;

  virtual void add_class(const NEWMAT::Matrix&) = 0;
  virtual void delete_empty_classes() = 0;

  // utils
  virtual double marglik(const NEWMAT::ColumnVector&,const int) = 0;
  virtual double margint(const NEWMAT::ColumnVector&) = 0;
  virtual double margint(const NEWMAT::ColumnVector&,const int) = 0;
  NEWMAT::ReturnMatrix get_dataindex();
  NEWMAT::ReturnMatrix calc_mldataindex();

  // getters
  std::vector<int> get_classnd()const{return m_classnd;}

  // spatial stuff
  void add_spatial_prior(const std::string& adjfile){
    NEWMAT::Matrix adj = MISCMATHS::read_ascii_matrix(adjfile);
    add_spatial_prior(adj);
  }
  void add_spatial_prior(const NEWMAT::Matrix& adj){
    m_spat = true;
    m_adj.ReSize(adj.Nrows(),adj.Ncols());
    m_adj  = adj;
  }
  void init_spatial();
  inline float get_spcparam()const{return m_spcparam;}
  void set_spcparam(const float& x){m_spcparam=x;}

  void set_projector(const NEWMAT::Matrix& P){
    m_reduced=true;
    m_projector=P;
  }
  NEWMAT::Matrix get_projector()const{return m_projector;}

  int get_numclass()const{return m_k;}
  void set_infinite(const bool isinf){infinite=isinf;}

  // io
  virtual void print_own_hyper() = 0;
  void print(std::ostream& os){
    os << "-------hyper-parameters-------"<<std::endl;
    os << "k      = "<<m_k<<std::endl;
    os << "alpha  = "<<m_alpha<<std::endl;
    os << "-------classes-------"<<std::endl;
    for(int i=0;i<m_k;i++){
      os <<m_classnd[i]<<" ";
    }
    os << std::endl;
    //print_own_hyper();
    os << std::endl;
  }
  virtual void save(const std::string&) = 0;
  virtual void record_own_params(const int) = 0;
  void record(const int samp){
    //cout<<"record sample "<<samp<<endl;
    m_sample_likelihood[samp] = m_likelihood;
    m_sample_k[samp]          = m_k;
    m_sample_alpha[samp]      = m_alpha;

    record_own_params(samp);
  }



};


class GWDPM_GibbsSampler : public DPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,GWDPM_GibbsSampler& g);

 protected:
  // class parameters
  std::vector<GaussianWishart>  m_gw;
  // hyperparameters       ------> estimated via gibb's sampling
  GaussianWishart          m_gw0;
  // hyper-hyperparameters ------> these are the only fixed parameters
  NEWMAT::ColumnVector     m_m0;         // m0 = mean(data)
  NEWMAT::SymmetricMatrix  m_S0;         // S0 = cov(data)
  NEWMAT::SymmetricMatrix  m_N0;         // inv(cov(data))/(nu0-d-1)^2
  int                      m_n0;
  // data-related quantities
  double                   m_margintbase;
  NEWMAT::ColumnVector     m_lambdabase;
  // records
  std::vector< std::vector<NEWMAT::ColumnVector> >    m_sample_means;
  std::vector< std::vector<NEWMAT::SymmetricMatrix> > m_sample_variances;
  std::vector< std::vector<float> >                   m_sample_proportions;

  // MAP estimations
  std::vector< NEWMAT::ColumnVector > m_map_means;
  std::vector< NEWMAT::SymmetricMatrix > m_map_variances;
  std::vector< float > m_map_proportions;


 public:
 GWDPM_GibbsSampler(const NEWMAT::Matrix& data,const int j,const int b,const int s):
  DPM_GibbsSampler(data,j,b,s){}
  GWDPM_GibbsSampler(const NEWMAT::Matrix& data):
  DPM_GibbsSampler(data){}
  ~GWDPM_GibbsSampler(){}

  // initialisation functions
  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_kmeans(const int);
  void init_max();
  void init_random(const int);

  // sample model parameters
  void sample_own_params();
  void sample_own_hypers();

  void mode_own_params();
  void mode_own_hypers();



  // utils
  double marglik(const NEWMAT::ColumnVector&,const int);
  double margint(const NEWMAT::ColumnVector&);
  double margint(const NEWMAT::ColumnVector&,const int);

  std::vector<NEWMAT::ColumnVector>    get_means()const;
  std::vector<NEWMAT::SymmetricMatrix> get_variances()const;
  std::vector<float>                   get_proportions()const;
  std::vector<NEWMAT::ColumnVector>    calc_posterior_means();
  std::vector<NEWMAT::SymmetricMatrix> calc_posterior_variances();
  std::vector<float>                   calc_posterior_proportions();

  void calc_posterior_stats();
  std::vector<NEWMAT::ColumnVector> get_map_means()const{return m_map_means;}
  std::vector<NEWMAT::SymmetricMatrix> get_map_variances()const{return m_map_variances;}
  std::vector<float> get_map_proportions()const{return m_map_proportions;}

  NEWMAT::ReturnMatrix calc_fstat();
  NEWMAT::ReturnMatrix calc_fstat(const std::vector<NEWMAT::ColumnVector>&,const std::vector<NEWMAT::SymmetricMatrix>&);
  float calc_fstat(const int& dim);
  float calc_meandiff(const int& dim);
  void project_classes(std::vector<NEWMAT::ColumnVector>&,std::vector<NEWMAT::SymmetricMatrix>&)const;



  double calc_bic();
  double calc_bic(const int& dim);
  double calc_rao_pvalue(const int& dim)const;


  void add_class(const NEWMAT::Matrix&);
  void delete_empty_classes();


  void save(const std::string& filename){
    std::string logsamples   = filename + ".samples";
    std::string logmeans     = filename + ".means";
    std::string logvariances = filename + ".variances";
    std::string zzz          = filename + ".z";
    std::string zmap         = filename + ".zmap";
    std::string stats        = filename + ".stats";
    std::string dat          = filename + ".data";

    std::ofstream of_s(logsamples.c_str());
    std::ofstream of_m(logmeans.c_str());
    std::ofstream of_v(logvariances.c_str());
    std::ofstream of_z(zzz.c_str());
    std::ofstream of_zmap(zmap.c_str());
    std::ofstream of_stats(stats.c_str());
    std::ofstream of_dat(dat.c_str());


    double evidence=0;
    double maxlog=0;

    of_s << "k\talpha\tlik\n";
    for (unsigned int i=0;i<m_sample_likelihood.size();i++){
      //OUT(i);
      of_s << m_sample_k[i]          << "\t"
	   << m_sample_alpha[i]      << "\t"
	   << m_sample_likelihood[i] << "\n";
      if(m_sample_likelihood[i]>maxlog || i==0)
	maxlog=m_sample_likelihood[i];
    }
    // compute evidence
    for(unsigned int i=0;i<m_sample_likelihood.size();i++){
      evidence += std::exp(m_sample_likelihood[i]-maxlog);
    }

    std::vector<NEWMAT::ColumnVector>    postmeans   = calc_posterior_means();
    std::vector<NEWMAT::SymmetricMatrix> postvars    = calc_posterior_variances();
    std::vector<float>                   postprop    = calc_posterior_proportions();
    std::cout<<"1"<<std::endl;

    NEWMAT::ColumnVector mlz(m_n);
    mlz = get_dataindex();
    of_z << mlz;
    // FSTATS
    NEWMAT::ColumnVector fs;
    if(!m_reduced)
      fs=calc_fstat(postmeans,postvars);
    else{
      std::vector<NEWMAT::ColumnVector> mu;
      std::vector<NEWMAT::SymmetricMatrix> sig;
      project_classes(mu,sig);
      fs=calc_fstat(mu,sig);
    }

    for(int k=0;k<m_k;k++){
      m_gw[k].set_smu(postmeans[k]);
      m_gw[k].set_ssigma(postvars[k]);
    }
    mlz = calc_mldataindex();
    of_zmap << mlz;

    // store means and variances
    for(int k=0;k<m_k;k++){
      of_m << postmeans[k].t();
      of_v << postvars[k];
    }

    of_stats << fs.t();

    // save data
    for(int i=1;i<=m_n;i++)
      of_dat << m_data.Row(i);

    evidence = -log((float)m_sample_likelihood.size()) + maxlog + log(evidence);
    std::cout<<"model (log)evidence  : " << evidence <<std::endl;
    std::cout<<"MAP k                : " << mlz.MaximumAbsoluteValue()+1<<std::endl;
    std::cout<<"Fstats               : " << fs.t();
  }

  void print_own_hyper(){
    for(int k=0;k<m_k;k++){
      std::cout<<"Params For Class " << k <<std::endl;
      std::cout << m_gw[k];
    }
    std::cout<<"HYPERPRIORS " <<std::endl;
    std::cout<<m_gw0;
  }
  void record_own_params(const int samp){
    m_sample_means[samp]       = get_means();
    m_sample_variances[samp]   = get_variances();
    m_sample_proportions[samp] = get_proportions();
  }

  std::vector<NEWMAT::ColumnVector> get_sample_mean(const int samp){return m_sample_means[samp];}


};


// Mixture of (multivariate) exponential distributions
class EXPDPM_GibbsSampler : public DPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,EXPDPM_GibbsSampler& g);

 protected:

  // parameters            ------> estimated via gibb's sampling
  std::vector<MDGamma>     m_mdg;
  // hyperparameters       ------> estimated via gibb's sampling
  MDGamma                  m_mdg0;
  // hyper-hyperparameters ------> these are the only fixed parameters

  // data-related quantities
  double                   m_margintbase;
  double                   m_maxloglik;

public:
 EXPDPM_GibbsSampler(const NEWMAT::Matrix& data,const int j,const int b,const int s):
  DPM_GibbsSampler(data,j,b,s){}
  ~EXPDPM_GibbsSampler(){}

  // initialisation functions
  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_kmeans(const int);
  void init_max();
  void init_random(const int);

  // sample model parameters
  void sample_own_params();
  void sample_own_hypers();

  // utils
  double marglik(const NEWMAT::ColumnVector&,const int);
  double marglik_debug(const NEWMAT::ColumnVector&,const int);
  double margint(const NEWMAT::ColumnVector&);
  double margint(const NEWMAT::ColumnVector&,const int){std::cout<<"Not implemented!"<<std::endl;exit(0);}

  void add_class(const NEWMAT::Matrix&);
  void delete_empty_classes();


  void save(const std::string& filename){
    std::string logsamples   = filename + ".samples";
    std::string logmeans     = filename + ".nabla";
    std::string zzz          = filename + ".z";

    std::ofstream of_s(logsamples.c_str());
    std::ofstream of_m(logmeans.c_str());
    std::ofstream of_z(zzz.c_str());

    double evidence=0;
    double maxlog=0;

    of_s << "k\talpha\tlik\n";
    for (unsigned int i=0;i<m_sample_likelihood.size();i++){
      //OUT(i);
      of_s << m_sample_k[i]          << "\t"
	   << m_sample_alpha[i]      << "\t"
	   << m_sample_likelihood[i] << "\n";
      if(m_sample_likelihood[i]>maxlog || i==0)
	maxlog=m_sample_likelihood[i];
    }
    // compute evidence
    for(unsigned int i=0;i<m_sample_likelihood.size();i++){
      evidence += std::exp(m_sample_likelihood[i]-maxlog);
    }

    // store gamma params
    for(int k=0;k<m_k;k++){
      of_m << m_mdg[k].get_smdg().t();
    }


    evidence = -std::log((float)m_sample_likelihood.size()) + maxlog + std::log(evidence);
    std::cout<<"current number of clusters: " << m_k <<std::endl;
    std::cout<<"model (log)evidence       : " << evidence <<std::endl;

    NEWMAT::ColumnVector mlz(m_n);
    mlz = calc_mldataindex();
    of_z << mlz;

    std::cout<<"MaximumLikelihood k       : "<< mlz.MaximumAbsoluteValue()+1<<std::endl;

  }

  void record_own_params(const int samp){}

  //io
  void print_own_hyper(){
    std::cout << m_mdg0;
  }


};

}

#endif
