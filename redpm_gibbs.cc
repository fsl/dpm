#include "redpm_gibbs.h"



void REDPM_GibbsSampler::init_own_params(){

  // scaling factor from maximum likelihood estimation
  // dealing wit numerical approximation
  m_maxloglik = 0;
  
  double loglik;
  for (int jj=0;jj<m_J;jj++){
    ColumnVector mlLambda = mean(m_data[jj],1).t();
    for (int i=1;i<=m_n[jj];i++){
      loglik = 0.0;
      for(int j=1;j<=m_d;j++){
	loglik += -log(mlLambda(j)) - m_data[jj](i,j)/mlLambda(j);
      }
      if(jj==0)m_maxloglik=loglik;
      if( loglik > m_maxloglik) m_maxloglik = loglik;
    }
  }


}

// one class per data point
// Jxn clusters in total
void REDPM_GibbsSampler::init_oneperdata(){
  cout << "One per data not implemented, sorry!" << endl;
  exit(0);
}
void REDPM_GibbsSampler::init_onebigclass(){
  cout << "One big class not implemented, sorry!" << endl;
  exit(0);
}
void REDPM_GibbsSampler::init_max(){

  // initialise z with maximum connectivity
  int indmax=0;
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      int index;
      m_data[j].Row(i+1).Maximum1(index);
      m_z[j][i] = index-1;
      indmax = index>indmax?index:indmax;
      OUT(m_z[j][i]);
    }
  }

  m_k = indmax;
  OUT(m_k);  
  //  exit(0);


  m_mdg.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_mdg[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }

  // init priors
  m_mdg0.clear();
  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  MDGamma tmpmdg(a,b);
  for(int k=0;k<m_k;k++) m_mdg0.push_back(tmpmdg);


  for(int k=0;k<m_k;k++){
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=0;i<m_n[j];i++){
	if(m_z[j][i] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
	indz++;
      }
      m_nperclass[j][k] = nj;
      
      MDGamma mdg(m_d);
      mdg.postupdate(dat,m_mdg0[k]);
      m_mdg[j][k] = mdg;
    }
    m_ntotperclass[k] = (int)dat.size();
  }

}
void REDPM_GibbsSampler::init_kmeans(const int k){

  // consider all data points in one big group and do kmeans
  m_k = k;
  Matrix alldata(m_ntot,m_d);
  int fr=1;
  vector<int> allz(m_ntot);
  for(int j=0;j<m_J;j++){
    alldata.SubMatrix(fr,fr+m_n[j]-1,1,m_d) = m_data[j];
    fr += m_n[j];
  }
  do_kmeans(alldata,allz,k);
  
  m_mdg.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_mdg[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }

  // init priors
  m_mdg0.clear();
  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  MDGamma tmpmdg(a,b);
  for(int k=0;k<m_k;k++) m_mdg0.push_back(tmpmdg);


  // update classes with data
  for(int k=1;k<=m_k;k++){
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=1;i<=m_n[j];i++){
	if(allz[indz] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i).t());
	  m_z[j][i-1] = k-1 ;
	}
	indz++;
      }
      m_nperclass[j][k-1] = nj;

      MDGamma mdg(m_d);
      mdg.postupdate(dat,m_mdg0[k-1]);
      m_mdg[j][k-1] = mdg;
    }
    m_ntotperclass[k-1] = (int)dat.size();
  }

}

void REDPM_GibbsSampler::init_random(const int k){
  // random assignment of data points to k classes
  m_k=k;
  for(int j=0;j<m_J;j++){
    vector< pair<float,int> > rindex(m_n[j]);
    randomise(rindex);
    vector< pair<float,int> >::iterator riter;
    int nn=0,cl=1,nperclass=(int)(float(m_n[j])/float(m_k));
    for(riter=rindex.begin();riter!=rindex.end();++riter){
      m_z[j][(*riter).second] = cl;
      nn++;
      if(nn>=nperclass && cl<m_k){
	nn=0;
	cl++;
      }
    }
  }


  // init priors
  m_mdg0.clear();
  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  MDGamma tmpmdg(a,b);
  for(int k=0;k<m_k;k++) m_mdg0.push_back(tmpmdg);


  m_mdg.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_mdg[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }

  for(int k=1;k<=m_k;k++){
    vector<ColumnVector> dat;
    for(int j=0;j<m_J;j++){
      int njk=0;
      for(int i=0;i<m_n[j];i++)
	if(m_z[j][i] == k){
	  m_z[j][i] --;
	  njk ++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
      m_nperclass[j][k-1] = njk;

      MDGamma mdg(m_d);
      mdg.postupdate(dat,m_mdg0[k-1]);
      m_mdg[j].push_back(mdg);
    }
    m_ntotperclass[k-1] = (int)dat.size();
  }
  
}



void REDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_ntotperclass[k] == 0){
      // delete this cluster from all groups
      for(int j=0;j<m_J;j++){
	for(int i=0;i<m_n[j];i++)
	  if(m_z[j][i]>k)m_z[j][i]--;
	for(int kk=k;kk<m_k-1;kk++){
	  m_nperclass[j][kk]=m_nperclass[j][kk+1];
	  m_mdg[j][kk]=m_mdg[j][kk+1];
	}
	m_nperclass[j].pop_back();
	m_mdg[j].pop_back();
      }
      for(int kk=k;kk<m_k-1;kk++){
	m_ntotperclass[kk]=m_ntotperclass[kk+1];
	m_nsubjperclass[kk]=m_nsubjperclass[kk+1];
      }
      m_ntotperclass.pop_back();
      m_nsubjperclass.pop_back();
      m_k--;
    }
    
  }// end loop on k
}

void REDPM_GibbsSampler::add_class(const Matrix& datapoint,const int j){

  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  MDGamma tmpmdg(a,b);
  m_mdg0.push_back(tmpmdg);



  MDGamma mdg(m_d);
  for(int jj=0;jj<m_J;jj++)
    m_mdg[jj].push_back(mdg);

  m_mdg[j][m_k-1].postupdate(datapoint,m_mdg0[m_k-1]);

}


void REDPM_GibbsSampler::sample_own_params(){
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++)
      data[ m_z[j][i] ].push_back(m_data[j].Row(i+1).t());
  
    for(int k=0;k<m_k;k++)
      if(data[k].size()>0){
	m_mdg[j][k].postupdate(data[k],m_mdg0[k]);

	//	cout << "======Prior " << k <<endl;
	//	cout << m_mdg[j][k];
      }
  }

}
void REDPM_GibbsSampler::sample_own_hypers(){
  ColumnVector a(m_d),b(m_d);
  a=1;b=0;
  for(int k=0;k<m_k;k++)
    b+=m_mdg0[k].get_b();
  MDGamma tmpmdg(float(m_k)*a,b);
  tmpmdg.sample();
  b = tmpmdg.get_b();

  // sample b in m_mdg0
  ColumnVector lambda(m_d);
  for(int k=0;k<m_k;k++){
    lambda=0.0;
    for(int j=0;j<m_J;j++){
      if(m_nperclass[j][k]>0)
	lambda += m_mdg[j][k].get_smdg();
    }
    lambda = lambda.Sum();

    MDGamma mdg(float(m_J*m_d)*m_mdg0[k].get_a()+a,lambda+b);
    m_mdg0[k].set_b(mdg.get_smdg());
    m_mdg0[k].sample();

    //    cout << "=======HyperPrior " << k << endl;
    //    cout << m_mdg0[k];
  }


}
double REDPM_GibbsSampler::marglik(const ColumnVector& data,const int j,const int k){
 double res=0.0;

  for(int i=1;i<=m_d;i++){
    res += log(m_mdg[j][k].get_smdg(i)) - m_mdg[j][k].get_smdg(i)*data(i);
  }


  return res-m_maxloglik;
}
double REDPM_GibbsSampler::margint(const ColumnVector& data){
  
  double res=0.0;
  ColumnVector a(m_d),b(m_d);
  a=0;b=0;
  for(int k=0;k<m_k;k++){
    a += m_mdg0[k].get_a();
    b += m_mdg0[k].get_b();
  }

  MDGamma mdg(a/float(m_k),b/float(m_k));
  
  for(int i=1;i<=m_d;i++){
    res += log(mdg.get_smdg(i)) - mdg.get_smdg(i)*data(i);
  }

  return res-m_maxloglik;
}


////// GAUSSIAN-WISHART STUFF
void RGDPM_GibbsSampler::init_own_params(){

  // init priors
  m_gw0.clear();
  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  GaussianWishart tmpmdg(a,b);
  for(int k=0;k<m_k;k++) m_gw0.push_back(tmpmdg);


}
void RGDPM_GibbsSampler::init_max(){
  // initialise z with maximum connectivity
  int indmax=0;
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      int index;
      m_data[j].Row(i+1).Maximum1(index);
      m_z[j][i] = index-1;
      indmax = index>indmax?index:indmax;
    }
  }
  m_k = indmax;
  m_gw.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_gw[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }
  for(int k=0;k<m_k;k++){
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=0;i<m_n[j];i++){
	if(m_z[j][i] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
	indz++;
      }
      m_nperclass[j][k] = nj;   
      GaussianWishart gw(m_d);
      gw.postupdate(dat,m_gw0[k]);
      m_gw[j][k] = mdg;
    }
    m_ntotperclass[k] = (int)dat.size();
  }
}
void RGDPM_GibbsSampler::init_kmeans(const int k){

  // consider all data points in one big group and do kmeans
  m_k = k;
  Matrix alldata(m_ntot,m_d);
  int fr=1;
  vector<int> allz(m_ntot);
  for(int j=0;j<m_J;j++){
    alldata.SubMatrix(fr,fr+m_n[j]-1,1,m_d) = m_data[j];
    fr += m_n[j];
  }
  do_kmeans(alldata,allz,k);
  
  m_gw.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_gw[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }
  // update classes with data
  for(int k=1;k<=m_k;k++){
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=1;i<=m_n[j];i++){
	if(allz[indz] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i).t());
	  m_z[j][i-1] = k-1 ;
	}
	indz++;
      }
      m_nperclass[j][k-1] = nj;

      GaussianWishart gw(m_d);
      gw.postupdate(dat,m_gw0[k-1]);
      m_gw[j][k-1] = gw;
    }
    m_ntotperclass[k-1] = (int)dat.size();
  }

}

void RGDPM_GibbsSampler::init_random(const int k){
  // random assignment of data points to k classes
  m_k=k;
  for(int j=0;j<m_J;j++){
    vector< pair<float,int> > rindex(m_n[j]);
    randomise(rindex);
    vector< pair<float,int> >::iterator riter;
    int nn=0,cl=1,nperclass=(int)(float(m_n[j])/float(m_k));
    for(riter=rindex.begin();riter!=rindex.end();++riter){
      m_z[j][(*riter).second] = cl;
      nn++;
      if(nn>=nperclass && cl<m_k){
	nn=0;
	cl++;
      }
    }
  }

  m_gw.resize(m_J);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_gw[j].resize(m_k);
    m_nperclass[j].resize(m_k);
  }

  for(int k=1;k<=m_k;k++){
    vector<ColumnVector> dat;
    for(int j=0;j<m_J;j++){
      int njk=0;
      for(int i=0;i<m_n[j];i++)
	if(m_z[j][i] == k){
	  m_z[j][i] --;
	  njk ++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
      m_nperclass[j][k-1] = njk;

      GaussianWishart gw(m_d);
      gw.postupdate(dat,m_gw0[k-1]);
      m_gw[j].push_back(gw);
    }
    m_ntotperclass[k-1] = (int)dat.size();
  }
  
}



void RGDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_ntotperclass[k] == 0){
      // delete this cluster from all groups
      for(int j=0;j<m_J;j++){
	for(int i=0;i<m_n[j];i++)
	  if(m_z[j][i]>k)m_z[j][i]--;
	for(int kk=k;kk<m_k-1;kk++){
	  m_nperclass[j][kk]=m_nperclass[j][kk+1];
	  m_gw[j][kk]=m_gw[j][kk+1];
	}
	m_nperclass[j].pop_back();
	m_gw[j].pop_back();
      }
      for(int kk=k;kk<m_k-1;kk++){
	m_ntotperclass[kk]=m_ntotperclass[kk+1];
	m_nsubjperclass[kk]=m_nsubjperclass[kk+1];
      }
      m_ntotperclass.pop_back();
      m_nsubjperclass.pop_back();
      m_k--;
    }
    
  }// end loop on k
}

void RGDPM_GibbsSampler::add_class(const Matrix& datapoint,const int j){

  ColumnVector a(m_d),b(m_d);
  a = 1.0; b = 1.0;
  GaussianWishart tmpmdg(a,b);
  m_gw.push_back(tmpmdg);


  GaussianWishart gw(m_d);
  for(int jj=0;jj<m_J;jj++)
    m_gw[jj].push_back(mdg);

  m_gw[j][m_k-1].postupdate(datapoint,m_gw0[m_k-1]);

}


void RGDPM_GibbsSampler::sample_own_params(){
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++)
      data[ m_z[j][i] ].push_back(m_data[j].Row(i+1).t());
  
    for(int k=0;k<m_k;k++)
      if(data[k].size()>0){
	m_gw[j][k].postupdate(data[k],m_gw0[k]);

	//	cout << "======Prior " << k <<endl;
	//	cout << m_mdg[j][k];
      }
  }

}
void RGDPM_GibbsSampler::sample_own_hypers(){

  SymmetricMatrix Nu0(m_d),A(m_d),S(m_d);
  ColumnVector a(m_d),mu0(m_d);    
  float B=0;
  float prior_shape = 1.0;
  float prior_scale = 1.0;

  for(int k=0;k<m_k;k++){
    A=0;a=0,B=0;
    for(int k=0;k<m_k;k++){
      S = m_gw[j][k].get_ssigma().i();
      a += S*m_gw[j][k].get_smu();
      A << A+S;
      B += ((m_gw[j][k].get_smu()-m_gw0[k].get_mu()).t()*S*(m_gw[j][k].get_smu()-m_gw0[k].get_mu())).AsScalar();
    }
    S << A+m_N0.i();
    A << (A+m_S0.i()).i();
    a = A*(a+m_S0.i()*m_m0);
  
    Nu0 = wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
    mu0 = mvnrnd(a.t(),A).t();
  
    m_gw0[k].set_Nu(Nu0);
    m_gw0[k].set_mu(mu0);


    Gamma G(prior_shape+m_J*m_d/2); //G.Set(rand()/float(RAND_MAX));
    m_gw0[k].set_kappa( (G.Next()/(prior_scale+B/2)));
  }
  

  


}
double RGDPM_GibbsSampler::marglik(const ColumnVector& data,const int j,const int k){
  double res=0.0;
  LogAndSign ld=(2*M_PI*m_gw[j][k].get_ssigma()).LogDeterminant();

  res -= 0.5*(ld.LogValue()
	      +((data-m_gw[j][k].get_smu()).t()
	      *m_gw[j][k].get_ssigma().i()
		*(data-m_gw[j][k].get_smu())).AsScalar());

  return res;

}
double RGDPM_GibbsSampler::margint(const ColumnVector& data){
  LogAndSign ld;
  double res=m_margintbase;

  float kk=m_gw0.get_kappa();
  res += float(m_d)/2*log(kk/(1+kk));

  
  ld = m_gw0.get_Nu().LogDeterminant();
  res += ld.LogValue()*m_gw0.get_dof()/2;
  

  SymmetricMatrix A(m_d);
  A << m_gw0.get_Nu()+m_gw0.get_kappa()/(1+m_gw0.get_kappa())*(data-m_gw0.get_mu())*(data-m_gw0.get_mu()).t();
  ld = A.LogDeterminant();
  res -= ld.LogValue()*(m_gw0.get_dof()+1)/2;
  res +=m_d/2*(log(m_gw0.get_kappa()/(1+m_gw0.get_kappa())));

  return res;


  double res=0.0;
  ColumnVector a(m_d),b(m_d);
  a=0;b=0;
  for(int k=0;k<m_k;k++){
    a += m_mdg0[k].get_a();
    b += m_mdg0[k].get_b();
  }

  GaussianWishart mdg(a/float(m_k),b/float(m_k));
  
  for(int i=1;i<=m_d;i++){
    res += log(mdg.get_smdg(i)) - mdg.get_smdg(i)*data(i);
  }

  return res-m_maxloglik;
}
