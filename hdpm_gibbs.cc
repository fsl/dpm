#include <stdlib.h>
#include <stdio.h>
#include <ostream>

#include "armawrap/newmat.h"
#include "newran/newran.h"
#include "miscmaths/miscmaths.h"

#include "hdpm_gibbs.h"

using namespace std;
using namespace NEWMAT;
using namespace NEWRAN;
using namespace MISCMATHS;

namespace DPM {

std::ostream& operator << (ostream& o,HDPM_GibbsSampler& g){
  g.print(o);
  return o;
}

void HDPM_GibbsSampler::print(ostream& os){
  os << "-------hyper-parameters-------"<<endl;
  os << "k      = "<<m_k<<endl;
  os << "alpha  = "<<m_alpha<<endl;
  os << "gamma  = "<<m_gamma<<endl;
  print_own_hyper();
  os << "-------class-parameters-------"<<endl;
  for(int k=0;k<m_k;k++){
    os << "cluster "<<k<<endl;
    os << m_ntotperclass[k]<<" ";
    os << m_nsubjperclass[k];
    os << endl;
  }
  os << endl;
  os << "------- classes per group -------" << endl;
  for(int j=0;j<m_J;j++){
    os << "group " << j << endl;
    for(int k=0;k<m_k;k++)
      os << m_nperclass[j][k] << " ";
    os << endl;
  }

  os << endl << endl << endl;
}

void HDPM_GibbsSampler::init_spatial(){
  m_logspatterm.resize(m_J);
  //m_spcparam = param;


  for(int j=0;j<m_J;j++){
    m_logspatterm[j]=0;
    if(m_spat){
      for(int i=1;i<=m_n[j];i++){
	for(int col=1;col<=m_adj[j].Ncols();col++){
	  if(m_adj[j](i,col)>0)
	    m_logspatterm[j] += ((m_z[j][i-1]!=m_z[j][(int)m_adj[j](i,col)-1])?1:0);
	}
      }
    }
  }

}

void HDPM_GibbsSampler::sample_parameters(){
  cout << *this;
  //cout << "sample z" << endl;
  sample_z();
  //swap_two_random_classes();
  //cout << "sample own params" << endl;
  sample_own_params();

}

void HDPM_GibbsSampler::sample_hyperparameters(const bool isburned){
  sample_own_hypers();
  sample_alpha();
  sample_gamma();
}

void HDPM_GibbsSampler::init(const int& numclass,const string& scheme){
  cout << "Initialisation" << endl;


  // fixed hyperparameters
  m_a0 = 1.0/10.0;
  m_b0 = 1.0/10.0;
  // initial DP hyperparameters
  m_alpha = 1.0;
  m_gamma = 0.0;

  m_k     = numclass;


  init_own_params();

  // class parameters
  if(scheme == "oneperdata")
    init_oneperdata();
  else if(scheme == "one")
    init_onebigclass();
  else if(scheme == "kmeans")
    init_kmeans(numclass);
  else if(scheme == "max")
    init_max();
  else if(scheme == "random")
    init_random(numclass);
  else{
    cerr << "Unknown Initialisation Scheme" << endl;
    exit(0);
  }

  m_nsubjperclass.resize(m_k);
  m_nclasspersubj.resize(m_J);
  for (int k=0;k<m_k;k++)m_nsubjperclass[k]=0;
  for (int j=0;j<m_J;j++)m_nclasspersubj[j]=0;

  for (int k=0;k<m_k;k++){
    for (int j=0;j<m_J;j++){
      if(m_nperclass[j][k] >0){
	m_nsubjperclass[k]++;
	m_nclasspersubj[j]++;
      }
    }
  }
  //  cout << *this;
  //exit(0);

  //m_nclasspersubj.push_back(m_k);



  // randomised index for loop over data items
  randindex.resize(m_J);
  for(int j=0;j<m_J;j++)
    randindex[j].resize(m_n[j]);

  cout << *this;
  //save_current("init");

  init_spatial();

}


// sampling
void HDPM_GibbsSampler::sample_z(){
  // screen groups in a random order
  vector< pair<float,int> > rind = randomise(m_J);
  vector< pair<float,int> >::iterator iter;
  for(iter=rind.begin();iter!=rind.end();++iter){
    sample_zj((*iter).second);
  }
  // delete empty classes
  delete_empty_classes();
}

void HDPM_GibbsSampler::sample_zj(const int j){

  ColumnVector datapoint(m_d);
  vector< pair<float,int> > rind = randomise(m_n[j]);
  vector< pair<float,int> >::iterator iter;

  for(iter=rind.begin(); iter!=rind.end(); ++iter){
    ColumnVector cumsum(m_k+1);
    ColumnVector spcterm(m_k+1);
    spcterm=0;

    datapoint=m_data[j].Row((*iter).second+1).t();
    int oldz=m_z[j][(*iter).second],newz=oldz;
    m_nperclass[j][oldz] -= 1;
    m_ntotperclass[oldz] -= 1;
    if(m_nperclass[j][oldz] < 1)
      m_nsubjperclass[oldz] -= 1;

    float numer1=(float)m_n[j]-1+m_alpha;
    float numer2=0.0;
    for(int it=0;it<m_k;it++)numer2+=float(m_ntotperclass[it]);numer2+=m_gamma;

    // compute class weights
    double sum=0.0;
    for(int k=0;k<m_k;k++){
      if(m_spat && k!=oldz){
	for(int neigh=1;neigh<=m_adj[j].Ncols();neigh++){
	  if(m_adj[j]((*iter).second+1,neigh)<=0)continue;
	  if(m_z[j][m_adj[j]((*iter).second+1,neigh)-1]==oldz)
	    spcterm(k+1) += 2;
	  else{
	    if(m_z[j][m_adj[j]((*iter).second+1,neigh)-1]==newz)
	      spcterm(k+1) -= 2;
	  }
	}
      }
      if(m_nperclass[j][k] > 0){
	sum += float(m_nperclass[j][k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / numer1;
	cumsum(k+1) = sum;
      }
      else{
	sum += m_alpha*float(m_ntotperclass[k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / (numer1*numer2);
	cumsum(k+1) = sum;
      }
    }
    if(m_spat)
      for(int neigh=1;neigh<=m_adj[j].Ncols();neigh++){
	if(m_adj[j]((*iter).second+1,neigh)<=0)continue;
	if(m_z[j][m_adj[j]((*iter).second+1,neigh)-1]==oldz)
	  spcterm(m_k+1) += 2;
      }
    // the weighting here should include the unused classes as well
    //OUT(sum);
    if(infinite)
      sum += m_alpha*m_gamma*exp(margint(datapoint)-spcterm(m_k+1)*m_spcparam/4) / (numer1*numer2);
    //OUT(m_alpha*m_gamma*exp(margint(datapoint)) / (numer1*numer2));

    cumsum(m_k+1) = sum;
    // sample z using the weights
    float U=rand()/float(RAND_MAX);
    U *= sum;
    for(int k=1;k<=m_k+1;k++){
      if(U<cumsum(k)){
	newz=k-1;
	break;
      }
    }
    m_z[j][(*iter).second] = newz;

    m_logspatterm[j] += spcterm(newz+1); // update spatial prior

    if( newz >= m_k ){ // add a new class
      //cout<<"created new class!!!!"<<endl;
      //OUT(cumsum.t());
      //exit(0);
      m_k++;
      for(int jj=0;jj<m_J;jj++){
	m_nperclass[jj].push_back(0);
      }
      m_nperclass[j][newz]=1;
      m_ntotperclass.push_back(1);
      m_nsubjperclass.push_back(1);
      add_class(datapoint.t(),j);
    }
    else{
      if(m_nperclass[j][newz]<1)
	m_nsubjperclass[newz] += 1;
      if(m_nsubjperclass[newz] > m_J){cout<<"3andak"<<endl;exit(0);}

      m_nperclass[j][newz] += 1;
      m_ntotperclass[newz] += 1;
    }

  }// end loop over data points

}


void HDPM_GibbsSampler::mode_z(){
  m_map_z.resize(m_J);
  for(int j=0;j<m_J;j++)
    mode_zj(j);
}
void HDPM_GibbsSampler::mode_zj(const int j){

  ColumnVector datapoint(m_d);
  vector< pair<float,int> > rind = randomise(m_n[j]);
  vector< pair<float,int> >::iterator iter;

  for(iter=rind.begin(); iter!=rind.end(); ++iter){
    ColumnVector w(m_k);
    ColumnVector spcterm(m_k);
    spcterm=0;

    datapoint=m_data[j].Row((*iter).second+1).t();
    int oldz=m_z[j][(*iter).second],newz=oldz;
    m_nperclass[j][oldz] -= 1;
    m_ntotperclass[oldz] -= 1;
    if(m_nperclass[j][oldz] < 1)
      m_nsubjperclass[oldz] -= 1;

    float numer1=(float)m_n[j]-1+m_alpha;
    float numer2=0.0;
    for(int it=0;it<m_k;it++)numer2+=float(m_ntotperclass[it]);numer2+=m_gamma;

    // compute class weights
    for(int k=0;k<m_k;k++){
      if(m_spat && k!=oldz){
	for(int neigh=1;neigh<=m_adj[j].Ncols();neigh++){
	  if(m_adj[j]((*iter).second+1,neigh)<=0)continue;
	  if(m_z[j][m_adj[j]((*iter).second+1,neigh)-1]==oldz)
	    spcterm(k+1) += 2;
	  else{
	    if(m_z[j][m_adj[j]((*iter).second+1,neigh)-1]==newz)
	      spcterm(k+1) -= 2;
	  }
	}
      }
      if(m_nperclass[j][k] > 0){
	w(k+1) = float(m_nperclass[j][k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / numer1;
      }
      else{
	w(k+1) = m_alpha*float(m_ntotperclass[k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / (numer1*numer2);
      }
    }
    w.Maximum1(newz);
    newz--;
    m_z[j][(*iter).second] = newz;

    if(m_nperclass[j][newz]<1)
      m_nsubjperclass[newz] += 1;
    if(m_nsubjperclass[newz] > m_J){cout<<"3andak"<<endl;exit(0);}
    m_nperclass[j][newz] += 1;
    m_ntotperclass[newz] += 1;


  }// end loop over data points

  m_map_z[j].ReSize(m_n[j]);
  for(int i=1;i<=m_n[j];i++){
    m_map_z[j](i) = m_z[j][i-1];
  }


}

void HDPM_GibbsSampler::mode_fuzzy_z(){
  m_map_fuzzy_z.resize(m_J);
  for(int j=0;j<m_J;j++)
    mode_fuzzy_zj(j);
}
void HDPM_GibbsSampler::mode_fuzzy_zj(const int j){

  ColumnVector datapoint(m_d);
  m_map_fuzzy_z[j].ReSize(m_n[j],m_k);

  for(int i=1;i<=m_n[j];i++){
    ColumnVector w(m_k);
    ColumnVector spcterm(m_k);
    spcterm=0;

    datapoint=m_data[j].Row(i).t();
    int oldz=m_z[j][i-1],newz=oldz;
    m_nperclass[j][oldz] -= 1;
    m_ntotperclass[oldz] -= 1;
    if(m_nperclass[j][oldz] < 1)
      m_nsubjperclass[oldz] -= 1;

    float numer1=(float)m_n[j]-1+m_alpha;
    float numer2=0.0;
    for(int it=0;it<m_k;it++)numer2+=float(m_ntotperclass[it]);numer2+=m_gamma;

    // compute class weights
    double wsum=0.0;
    for(int k=0;k<m_k;k++){
      if(m_spat && k!=oldz){
	for(int neigh=1;neigh<=m_adj[j].Ncols();neigh++){
	  if(m_adj[j](i,neigh)<=0)continue;
	  if(m_z[j][m_adj[j](i,neigh)-1]==oldz)
	    spcterm(k+1) += 2;
	  else{
	    if(m_z[j][m_adj[j](i,neigh)-1]==newz)
	      spcterm(k+1) -= 2;
	  }
	}
      }
      if(m_nperclass[j][k] > 0){
	m_map_fuzzy_z[j](i,k+1) = float(m_nperclass[j][k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / numer1;
	wsum += m_map_fuzzy_z[j](i,k+1);
      }
      else{
	m_map_fuzzy_z[j](i,k+1) = m_alpha*float(m_ntotperclass[k])*exp(marglik(datapoint,j,k)-spcterm(k+1)*m_spcparam/4) / (numer1*numer2);
	wsum += m_map_fuzzy_z[j](i,k+1);
      }
    }

    // normalise
    if(wsum>0)
      m_map_fuzzy_z[j].Row(i) /= wsum;


    if(m_nperclass[j][oldz]<1)
      m_nsubjperclass[oldz] += 1;
    if(m_nsubjperclass[oldz] > m_J){cout<<"3andak"<<endl;exit(0);}
    m_nperclass[j][oldz] += 1;
    m_ntotperclass[oldz] += 1;


  }// end loop over data points


}


void HDPM_GibbsSampler::sample_alpha(){

  ColumnVector w(m_J);
  ColumnVector s(m_J);

  //  OUT(m_alpha);

  float x,y,mtotal=0.0;
  Gamma g1(m_alpha+1);
  for(int j=1;j<=m_J;j++){
    //OUT(m_nclasspersubj[j-1]);
    mtotal += float(m_nclasspersubj[j-1]);
    Gamma g2(m_n[j-1]);
    x=g1.Next();
    y=g2.Next();
    //OUT(x);OUT(y);
    w(j) = log(x/(x+y));
    s(j) = ((rand()/float(RAND_MAX) <= float(m_n[j-1])/m_alpha)?1:0);
  }
  //OUT(s.t());
  //OUT(w.t());

  Gamma G(m_a0+mtotal-s.Sum());

  m_alpha=G.Next()/(m_b0-w.Sum());

}
void HDPM_GibbsSampler::sample_gamma(){
  float eta,prop;
  float ak=m_a0+m_k-1,bn;

  Gamma G1(ak+1);
  Gamma G2(ak);
  Gamma B1(m_gamma+1);
  Gamma B2(m_k);

  eta  = B1.Next();
  eta /= (eta+B2.Next());
  bn   = m_b0-std::log(eta);

  prop=ak/(ak+m_k*bn);
  m_gamma=(prop*G1.Next()+(1-prop)*G2.Next())/bn;
  //m_gamma=0.0;

}


vector<ColumnVector> HDPM_GibbsSampler::get_dataindex(){
  vector<ColumnVector> res(m_J);
  for(int j=0;j<m_J;j++){
    ColumnVector index(m_n[j]);
    for(int i=0;i<m_n[j];i++)
      index(i+1) = m_z[j][i];
    res[j]=index;
  }
  return res;
}
ReturnMatrix HDPM_GibbsSampler::get_dataindex(int j){
    ColumnVector index(m_n[j]);
    for(unsigned int i=0;i<m_z[j].size();i++)
      index(i+1) = m_z[j][i];
    index.Release();
    return index;
}
ReturnMatrix HDPM_GibbsSampler::calc_mldataindex(int j){
  ColumnVector index(m_n[j]);
  double lik,tmplik;
  for(int i=1;i<=m_n[j];i++){
    lik=0.0;tmplik=0;index(i) = 0;
    for(int k=0;k<m_k;k++){
      //tmplik = exp(log(m_nperclass[j][k])+marglik(m_data[j].Row(i).t(),j,k));
      tmplik = exp(marglik(m_data[j].Row(i).t(),j,k));
      if(tmplik>lik || k==0){
	lik = tmplik;
	index(i) = k;
      }
    }
  }
  index.Release();
  return index;
}
ReturnMatrix HDPM_GibbsSampler::calc_grpdataindex(){
  for(int j=0;j<m_J;j++){
    if(m_n[j]!=m_n[0]){
      cerr << "groups have different sizes; can't produce group map" << endl;
      exit(0);
    }
  }
  Matrix indexall(m_n[0],m_J);
  Matrix counter(m_n[0],m_k);

  for(int j=0;j<m_J;j++)
    indexall.Column(j+1) = get_dataindex(j);

  counter=0.0;
  for(int i=1;i<=m_n[0];i++)
    for(int j=1;j<=m_J;j++)
      counter(i,indexall(i,j)+1) ++;


  ColumnVector index(m_n[0]);int indmax;
  for(int i=1;i<=m_n[0];i++){
    counter.Row(i).Maximum1(indmax);
    index(i) = indmax-1;
  }

  index.Release();
  return index;
}
double HDPM_GibbsSampler::calc_loglikelihood(){
  double loglik=0;
  for(int j=0;j<m_J;j++){
    for(int i=1;i<=m_n[j];i++){
      int k = m_z[j][i];
      loglik += log(m_nperclass[j][k])+marglik(m_data[j].Row(i).t(),j,k);
    }
  }

  return loglik;
}

double HDPM_GibbsSampler::calc_energy(){
  double en=0;
  en -= calc_logprior();
  en -= calc_loglikelihood();

  return en;

}
// GAUSSIAN-WISHART STUFF

void GWHDPM_GibbsSampler::init_own_params(){

  m_S0 << 1000.0*IdentityMatrix(m_d);
  m_N0 << IdentityMatrix(m_d);
  m_n0 = 1;

  m_m0.ReSize(m_d);
  m_m0 = 0;
  int ntot=0;
  for(int j=0;j<m_J;j++)
    for(int i=1;i<=m_n[j];i++){
      m_m0 += m_data[j].Row(i).t();
      ntot++;
    }
  m_m0 /= float(ntot);

  // class hyperparameters
  float kappa0 = 1.0;
  int   nu0    = m_d;
  SymmetricMatrix Nu0(m_d);
  Nu0 << m_n0*m_N0;
  ColumnVector mu0(m_d);
  mu0 = m_m0;


  m_gw0 = GaussianWishart(mu0,Nu0,nu0,kappa0);

  // calculate part of the marginalisation over class mean/variance
  // this part doesn't change through the iterations
  m_margintbase = m_d/2*(-log(M_PI))
    + lgam(float(nu0+1)/2.0) -lgam(float(nu0+1-m_d)/2.0);

  // allocate some space for samples
  m_sample_means.resize(m_nsamples);
  m_sample_variances.resize(m_nsamples);
  m_sample_proportions.resize(m_nsamples);


}

void GWHDPM_GibbsSampler::init_oneperdata(){
  // one class per data point
  // Jxn clusters in total
  m_k = m_ntot;
  int k=0;
  m_gw.clear();
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      m_nperclass[j].push_back(1);
      m_ntotperclass.push_back(1);
      m_z[j][i] = k;
      k++;

      GaussianWishart gw(m_d);
      gw.postupdate(m_data[j].Row(i+1).t(),m_gw0);
      m_gw.push_back(gw);
    }
  }
}
void GWHDPM_GibbsSampler::init_onebigclass(){
  m_k = 1;
  vector<ColumnVector> dat;
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      dat.push_back(m_data[j].Row(i+1).t());
      m_z[j][i] = 0;
    }
    m_nperclass[j].push_back(m_n[j]);
  }
  m_ntotperclass.push_back(m_ntot);

  GaussianWishart gw(m_d);
  gw.postupdate(dat,m_gw0);
  m_gw.clear();
  m_gw.push_back(gw);
}
void GWHDPM_GibbsSampler::init_max(){cerr<<"init max not implemented"<<endl;exit(0);}
void GWHDPM_GibbsSampler::init_kmeans(const int k){
  // consider all data points in one big group and do kmeans
  m_k = k;
  Matrix alldata(m_ntot,m_d);
  int fr=1;
  vector<int> allz(m_ntot);
  for(int j=0;j<m_J;j++){
    alldata.SubMatrix(fr,fr+m_n[j]-1,1,m_d) = m_data[j];
    fr += m_n[j];
  }
  do_kmeans(alldata,allz,m_k);

  m_gw.resize(m_k);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++)
    m_nperclass[j].resize(m_k);

  for(int k=1;k<=m_k;k++){
    GaussianWishart gw(m_d);
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=1;i<=m_n[j];i++){
	if(allz[indz] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i).t());
	  m_z[j][i-1] = k-1 ;
	}
	indz++;
      }
      m_nperclass[j][k-1] = nj;
    }
    gw.postupdate(dat,m_gw0);
    m_gw[k-1] = gw;
    m_ntotperclass[k-1] = (int)dat.size();
  }

}

void GWHDPM_GibbsSampler::init_random(const int k){
  // random assignment of data points to k classes
  m_k=k;
  for(int j=0;j<m_J;j++){
    vector< pair<float,int> > rindex(m_n[j]);
    randomise(rindex);
    vector< pair<float,int> >::iterator riter;
    int nn=0,cl=1,nperclass=(int)(float(m_n[j])/float(m_k));
    for(riter=rindex.begin();riter!=rindex.end();++riter){
      m_z[j][(*riter).second] = cl;
      nn++;
      if(nn>=nperclass && cl<m_k){
	nn=0;
	cl++;
      }
    }
  }


  m_gw.resize(m_k);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++)
    m_nperclass[j].resize(m_k);

  for(int k=1;k<=m_k;k++){
    GaussianWishart gw(m_d);
    vector<ColumnVector> dat;
    for(int j=0;j<m_J;j++){
      int njk=0;
      for(int i=0;i<m_n[j];i++)
	if(m_z[j][i] == k){
	  m_z[j][i] --;
	  njk ++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
      m_nperclass[j][k-1] = njk;
    }
    m_ntotperclass[k-1] = (int)dat.size();
    gw.postupdate(dat,m_gw0);
    m_gw[k-1] = gw;
  }

}

void GWHDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_ntotperclass[k] == 0){
      // delete this cluster from all groups
      for(int j=0;j<m_J;j++){
	for(int i=0;i<m_n[j];i++)
	  if(m_z[j][i]>k)m_z[j][i]--;
	for(int kk=k;kk<m_k-1;kk++){
	  m_nperclass[j][kk]=m_nperclass[j][kk+1];
	}
	m_nperclass[j].pop_back();
      }
      for(int kk=k;kk<m_k-1;kk++){
	m_ntotperclass[kk]=m_ntotperclass[kk+1];
	m_nsubjperclass[kk]=m_nsubjperclass[kk+1];
	m_gw[kk]=m_gw[kk+1];
      }
      m_ntotperclass.pop_back();
      m_nsubjperclass.pop_back();
      m_gw.pop_back();
      m_k--;
    }

  }// end loop on k
  for(int j=0;j<m_J;j++){
    m_nclasspersubj[j]=0;
    for(int k=0;k<m_k;k++)
      if(m_nperclass[j][k]>0)
	m_nclasspersubj[j]++;
  }
}

void GWHDPM_GibbsSampler::add_class(const Matrix& datapoint){
  GaussianWishart gw(m_d);
  gw.postupdate(datapoint,m_gw0);
  m_gw.push_back(gw);
}


void GWHDPM_GibbsSampler::sample_own_params(){
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int j=0;j<m_J;j++)
    for(int i=0;i<m_n[j];i++){
      data[ m_z[j][i] ].push_back(m_data[j].Row(i+1).t());
    }
  for(int k=0;k<m_k;k++)
    if(data[k].size()>0)
      m_gw[k].postupdate(data[k],m_gw0);
}

void GWHDPM_GibbsSampler::mode_own_params(){
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int j=0;j<m_J;j++)
    for(int i=0;i<m_n[j];i++){
      data[ m_z[j][i] ].push_back(m_data[j].Row(i+1).t());
    }
  for(int k=0;k<m_k;k++)
    if(data[k].size()>0)
      m_gw[k].postmode(data[k],m_gw0);
}



void GWHDPM_GibbsSampler::sample_own_hypers(){
  SymmetricMatrix Nu0(m_d),AA(m_d),A(m_d),S(m_d);
  ColumnVector a(m_d),mu0(m_d);
  float B=0;

  A=0;a=0;AA=0;
  for(int k=0;k<m_k;k++){
    S = m_gw[k].get_ssigma().i();
    a += m_gw[k].get_kappa()*S*m_gw[k].get_smu();
    A << A+m_gw[k].get_kappa()*S;
    AA << AA+S;
    B += ((m_gw[k].get_smu()-m_gw0.get_mu()).t()*S*(m_gw[k].get_smu()-m_gw0.get_mu())).AsScalar();
  }
  S << AA+m_N0.i();
  A << (A+m_S0.i()).i();
  a = A*(a+m_S0.i()*m_m0);

  Nu0 = DPM::wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  mu0 = MISCMATHS::mvnrnd(a.t(),A).t();

  m_gw0.set_Nu(Nu0);
  m_gw0.set_mu(mu0);


  float prior_shape = 1.0;
  float prior_scale = 1.0;

  Gamma G(prior_shape+m_k*m_d/2); //G.Set(rand()/float(RAND_MAX));
  m_gw0.set_kappa( (G.Next()/(prior_scale+B/2)));
  //m_gw0.set_kappa(10.0);

}



void GWHDPM_GibbsSampler::mode_own_hypers(){
  SymmetricMatrix Nu0(m_d),AA(m_d),A(m_d),S(m_d);
  ColumnVector a(m_d),mu0(m_d);
  float B=0;

  A=0;a=0;AA=0;
  for(int k=0;k<m_k;k++){
    S = m_gw[k].get_ssigma().i();
    a += m_gw[k].get_kappa()*S*m_gw[k].get_smu();
    A << A+m_gw[k].get_kappa()*S;
    AA << AA+S;
    B += ((m_gw[k].get_smu()-m_gw0.get_mu()).t()*S*(m_gw[k].get_smu()-m_gw0.get_mu())).AsScalar();
  }
  S << AA+m_N0.i();
  A << (A+m_S0.i()).i();
  a = A*(a+m_S0.i()*m_m0);


  Nu0 = DPM::wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  mu0 = MISCMATHS::mvnrnd(a.t(),A).t();

  //Nu0 = (m_k+1)*m_gw0.get_dof() * S.i();//wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  //mu0 = a;//mvnrnd(a.t(),A).t();


  m_gw0.set_Nu(Nu0);
  m_gw0.set_mu(mu0);

  float prior_shape = 1.0;
  float prior_scale = 1.0;

  Gamma G(prior_shape+float(m_k*m_d)/2); //G.Set(rand()/float(RAND_MAX));
  m_gw0.set_kappa( (G.Next()/(prior_scale+B/2)));
  //m_gw0.set_kappa(1.0);

  m_gw0.mode();

}

double GWHDPM_GibbsSampler::marglik(const ColumnVector& data,const int k){
  double res=0.0;
  LogAndSign ld=(2*M_PI*m_gw[k].get_ssigma()).LogDeterminant();

  res -= 0.5*(ld.LogValue()
	      +((data-m_gw[k].get_smu()).t()
	      *m_gw[k].get_ssigma().i()
		*(data-m_gw[k].get_smu())).AsScalar());

  return res;
}
double GWHDPM_GibbsSampler::margint(const ColumnVector& data){
  LogAndSign ld;
  double res=m_margintbase;

  float kk=m_gw0.get_kappa();
  res += float(m_d)/2*log(kk/(1+kk));


  ld = m_gw0.get_Nu().LogDeterminant();
  res += ld.LogValue()*m_gw0.get_dof()/2;


  SymmetricMatrix A(m_d);
  A << m_gw0.get_Nu()+m_gw0.get_kappa()/(1+m_gw0.get_kappa())*(data-m_gw0.get_mu())*(data-m_gw0.get_mu()).t();
  ld = A.LogDeterminant();
  res -= ld.LogValue()*(m_gw0.get_dof()+1)/2;
  res +=m_d/2*(log(m_gw0.get_kappa()/(1+m_gw0.get_kappa())));

  return res;
}

vector<ColumnVector> GWHDPM_GibbsSampler::get_means()const{
  vector<ColumnVector> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(m_gw[k].get_smu());
  return ret;
}
vector<SymmetricMatrix> GWHDPM_GibbsSampler::get_variances()const{
  vector<SymmetricMatrix> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(m_gw[k].get_ssigma());
  return ret;
}
vector<float> GWHDPM_GibbsSampler::get_proportions()const{
  vector<float> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(float(m_ntotperclass[k])/float(m_ntot));
  return ret;
}
vector<ColumnVector> GWHDPM_GibbsSampler::calc_posterior_means(){
  vector<ColumnVector> ret;
  for(int k=0;k<m_k;k++){
    ColumnVector v(m_d);
    v=0.0;
    ret.push_back(v);
  }

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++){
    if((int)m_sample_means[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_means[samp][k];
    }
  }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}
vector<SymmetricMatrix> GWHDPM_GibbsSampler::calc_posterior_variances(){
  vector<SymmetricMatrix> ret;
  for(int k=0;k<m_k;k++){
    SymmetricMatrix M(m_d);
    M=0.0;
    ret.push_back(M);
  }

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++)
    if((int)m_sample_variances[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_variances[samp][k];
    }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}
vector<float> GWHDPM_GibbsSampler::calc_posterior_proportions(){
  vector<float> ret;
  for(int k=0;k<m_k;k++)ret.push_back(0.0);

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++)
    if((int)m_sample_proportions[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_proportions[samp][k];
    }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}

ReturnMatrix GWHDPM_GibbsSampler::calc_fstat(){
  Matrix fstat;

  if(m_reduced){
    vector<ColumnVector> mu;
    vector<SymmetricMatrix> sig;
    project_classes(mu,sig);
    fstat=calc_fstat(mu,sig);
  }
  else{
    fstat.ReSize(m_d,2);
    for(int j=1;j<=m_d;j++){
      fstat(j,1) = calc_meandiff(j);
      fstat(j,2) = calc_fstat(j);
    }
  }

  fstat.Release();
  return(fstat);
}
ReturnMatrix GWHDPM_GibbsSampler::calc_fstat(const vector<ColumnVector>& means,const vector<SymmetricMatrix>& vars){
  Matrix fstat;
  float m,v,ss;

  fstat.ReSize(means[0].Nrows(),2);
  for(int j=1;j<=means[0].Nrows();j++){
    m=0.0;v=0.0,ss=0.0;
    for(int k=0;k<m_k;k++){
      ss += means[k](j)*means[k](j);
      m  += means[k](j);
      v  += vars[k](j,j);
    }
    fstat(j,1) = (ss - m*m/float(m_k));
    fstat(j,2) = (ss - m*m/float(m_k))/(float(m_k)*v);

  }
  fstat.Release();
  return(fstat);
}

  float GWHDPM_GibbsSampler::calc_fstat(const int& dim){
    float f;
    float m,v,ss;
    m=0.0;v=0.0;ss=0.0;

    for(int k=0;k<m_k;k++){
      ss += m_gw[k].get_smu()(dim)*m_gw[k].get_smu()(dim);
      m  += m_gw[k].get_smu()(dim);
      v  += m_gw[k].get_ssigma()(dim,dim);
    }
    f = (ss - m*m/float(m_k))/(float(m_k)*v);

    return(f);
  }
  float GWHDPM_GibbsSampler::calc_meandiff(const int& dim){
    float f;
    float m,ss;
    m=0.0;ss=0.0;

    for(int k=0;k<m_k;k++){
      ss += m_gw[k].get_smu()(dim)*m_gw[k].get_smu()(dim);
      m  += m_gw[k].get_smu()(dim);
    }
    f = (ss - m*m/float(m_k));

    return(f);
}

void GWHDPM_GibbsSampler::project_classes(vector<ColumnVector>& mu,vector<SymmetricMatrix>& sig)const{

//     if(m_projector.Nrows()==m_d){
//       m_projector = m_projector.t();
//     }

    mu.clear();sig.clear();
    ColumnVector      X(m_projector.Nrows());
    SymmetricMatrix   S(m_projector.Nrows());

    for(int k=0;k<m_k;k++){
      X = m_projector*m_gw[k].get_smu();
      S << m_projector*m_gw[k].get_ssigma()*m_projector.t();

      mu.push_back(X);
      sig.push_back(S);
    }

  }

void GWHDPM_GibbsSampler::calc_posterior_stats(){

  mode_own_hypers();
  mode_own_params();
  mode_z();
  mode_fuzzy_z();

  m_map_means       = get_means();
  m_map_variances   = get_variances();
  m_map_proportions = get_proportions();
}




  ReturnMatrix GWHDPM_GibbsSampler::calc_zmap(const int& j){
    ColumnVector zmap(m_n[j]);

    vector<ColumnVector>    postmeans   = calc_posterior_means();
    vector<SymmetricMatrix> postvars    = calc_posterior_variances();
    vector<float>           postprop    = calc_posterior_proportions();


    for(int k=0;k<m_k;k++){
      m_gw[k].set_smu(postmeans[k]);
      m_gw[k].set_ssigma(postvars[k]);
    }
    zmap = calc_mldataindex(j);

    zmap.Release();
    return zmap;
  }

// EXPONENTIAL STUFF

void EXPHDPM_GibbsSampler::init_own_params(){
  ColumnVector a(m_d),b(m_d);
  a = 1.0;
  b = 1.0;

  m_mdg0 = MDGamma(a,b);

  // calculate part of the marginalisation over class mean/variance
  // this part doesn't change through the iterations
  m_margintbase=0.0;
  for(int i=1;i<=m_d;i++)
    m_margintbase += log(m_mdg0.get_a(i));

  // scaling factor from maximum likelihood estimation
  // dealing wit numerical approximation
  m_maxloglik = 0;

  double loglik;
  for (int jj=0;jj<m_J;jj++){
    ColumnVector mlLambda = mean(m_data[jj],1).t();
    for (int i=1;i<=m_n[jj];i++){
      loglik = 0.0;
      for(int j=1;j<=m_d;j++){
	loglik += -log(mlLambda(j)) - m_data[jj](i,j)/mlLambda(j);
      }
      if(jj==0)m_maxloglik=loglik;
      if( loglik > m_maxloglik) m_maxloglik = loglik;
    }
  }

}

void EXPHDPM_GibbsSampler::init_oneperdata(){
  // one class per data point
  // Jxn clusters in total
  m_k = m_ntot;
  int k=0;
  m_mdg.clear();
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      m_nperclass[j].push_back(1);
      m_ntotperclass.push_back(1);
      m_z[j][i] = k;
      k++;

      MDGamma mdg(m_d);
      mdg.postupdate(m_data[j].Row(i+1).t(),m_mdg0);
      m_mdg.push_back(mdg);
    }
  }
}
void EXPHDPM_GibbsSampler::init_onebigclass(){
  m_k = 1;
  vector<ColumnVector> dat;
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      dat.push_back(m_data[j].Row(i+1).t());
      m_z[j][i] = 0;
    }
    m_nperclass[j].push_back(m_n[j]);
  }
  m_ntotperclass.push_back(m_ntot);

  MDGamma mdg(m_d);
  mdg.postupdate(dat,m_mdg0);
  m_mdg.clear();
  m_mdg.push_back(mdg);
}
void EXPHDPM_GibbsSampler::init_max(){

  // initialise z with maximum connectivity
  int indmax=0;
  for(int j=0;j<m_J;j++){
    for(int i=0;i<m_n[j];i++){
      int index;
      m_data[j].Row(i+1).Maximum1(index);
      m_z[j][i] = index-1;
      indmax = ((index>indmax)?index:indmax);
    }
  }

  m_k = indmax;


  m_mdg.resize(m_k);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_nperclass[j].resize(m_k);
  }
  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=1;i<=m_n[j];i++){
	if(m_z[j][i-1] == k-1){
	  nj++;
	  dat.push_back(m_data[j].Row(i).t());
	}
	indz++;
      }
      m_nperclass[j][k-1] = nj;
    }
    mdg.postupdate(dat,m_mdg0);
    m_mdg[k-1] = mdg;
    m_ntotperclass[k-1] = (int)dat.size();
  }



}
void EXPHDPM_GibbsSampler::init_kmeans(const int k){
  // consider all data points in one big group and do kmeans
  m_k = k;
  Matrix alldata(m_ntot,m_d);
  int fr=1;
  vector<int> allz(m_ntot);
  for(int j=0;j<m_J;j++){
    alldata.SubMatrix(fr,fr+m_n[j]-1,1,m_d) = m_data[j];
    fr += m_n[j];
  }
  do_kmeans(alldata,allz,m_k);

  m_mdg.resize(m_k);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++){
    m_nperclass[j].resize(m_k);
  }
  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    int nj,indz=0;
    for(int j=0;j<m_J;j++){
      nj=0;
      for(int i=1;i<=m_n[j];i++){
	if(allz[indz] == k){
	  nj++;
	  dat.push_back(m_data[j].Row(i).t());
	  m_z[j][i-1] = k-1 ;
	}
	indz++;
      }
      m_nperclass[j][k-1] = nj;
    }
    mdg.postupdate(dat,m_mdg0);
    m_mdg[k-1] = mdg;
    m_ntotperclass[k-1] = (int)dat.size();
  }

}

void EXPHDPM_GibbsSampler::init_random(const int k){
  // random assignment of data points to k classes
  m_k=k;
  for(int j=0;j<m_J;j++){
    vector< pair<float,int> > rindex(m_n[j]);
    randomise(rindex);
    vector< pair<float,int> >::iterator riter;
    int nn=0,cl=1,nperclass=(int)(float(m_n[j])/float(m_k));
    for(riter=rindex.begin();riter!=rindex.end();++riter){
      m_z[j][(*riter).second] = cl;
      nn++;
      if(nn>=nperclass && cl<m_k){
	nn=0;
	cl++;
      }
    }
  }


  m_mdg.resize(m_k);
  m_ntotperclass.resize(m_k);
  for(int j=0;j<m_J;j++)
    m_nperclass[j].resize(m_k);

  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    for(int j=0;j<m_J;j++){
      int njk=0;
      for(int i=0;i<m_n[j];i++)
	if(m_z[j][i] == k){
	  m_z[j][i] --;
	  njk ++;
	  dat.push_back(m_data[j].Row(i+1).t());
	}
      m_nperclass[j][k-1] = njk;
    }
    m_ntotperclass[k-1] = (int)dat.size();
    mdg.postupdate(dat,m_mdg0);
    m_mdg[k-1] = mdg;
  }

}

void EXPHDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_ntotperclass[k] == 0){
      // delete this cluster from all groups
      for(int j=0;j<m_J;j++){
	for(int i=0;i<m_n[j];i++)
	  if(m_z[j][i]>k)m_z[j][i]--;
	for(int kk=k;kk<m_k-1;kk++){
	  m_nperclass[j][kk]=m_nperclass[j][kk+1];
	}
	m_nperclass[j].pop_back();
      }
      for(int kk=k;kk<m_k-1;kk++){
	m_ntotperclass[kk]=m_ntotperclass[kk+1];
	m_nsubjperclass[kk]=m_nsubjperclass[kk+1];
	m_mdg[kk]=m_mdg[kk+1];
      }
      m_ntotperclass.pop_back();
      m_nsubjperclass.pop_back();
      m_mdg.pop_back();
      m_k--;
    }

  }// end loop on k
}

void EXPHDPM_GibbsSampler::add_class(const Matrix& datapoint){
  MDGamma mdg(m_d);
  mdg.postupdate(datapoint,m_mdg0);
  m_mdg.push_back(mdg);
}


void EXPHDPM_GibbsSampler::sample_own_params(){
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int j=0;j<m_J;j++)
    for(int i=0;i<m_n[j];i++){
      data[ m_z[j][i] ].push_back(m_data[j].Row(i+1).t());
    }
  for(int k=0;k<m_k;k++)
    if(data[k].size()>0){
      m_mdg[k].postupdate(data[k],m_mdg0);
    }
}
void EXPHDPM_GibbsSampler::sample_own_hypers(){
//   // sample b in m_mdg0
  ColumnVector lambda(m_d);
  lambda = 0.0;
  for(int k=0;k<m_k;k++)
    lambda += m_mdg[k].get_smdg();
  lambda = lambda.Sum();

  //  MDGamma mdg(float(m_k*m_d)*m_mdg0.get_a(),lambda);
  MDGamma mdg(float(m_k)*m_mdg0.get_a(),lambda);

  m_mdg0.set_b(mdg.get_smdg());
  m_mdg0.sample();


}
double EXPHDPM_GibbsSampler::marglik(const ColumnVector& data,const int k){
 double res=0.0;

  for(int i=1;i<=m_d;i++){
    res += log(m_mdg[k].get_smdg(i)) - m_mdg[k].get_smdg(i)*data(i);
  }


  return res-m_maxloglik;
}
double EXPHDPM_GibbsSampler::margint(const ColumnVector& data){
  //double res=m_margintbase;

  //for(int i=1;i<=m_d;i++)
  //res += m_mdg0.get_a(i)*log(m_mdg0.get_b(i)) -
  //  (m_mdg0.get_a(i)+1) * log(m_mdg0.get_b(i)+data(i));

  //  OUT(m_maxloglik);
  //  OUT(res-m_maxloglik);


  // sample from the prior distribution

  double res=0.0;
  MDGamma mdg(m_mdg0.get_a(),m_mdg0.get_b());

  for(int i=1;i<=m_d;i++){
    res += log(mdg.get_smdg(i)) - mdg.get_smdg(i)*data(i);
  }

  return res-m_maxloglik;
}


double EXPHDPM_GibbsSampler::calc_logprior(){
  return 0;
}

}
