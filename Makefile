include $(FSLCONFDIR)/default.mk

PROJNAME = dpm
SOFILES  = libfsl-dpm.so
LIBS     = -lfsl-newimage  -lfsl-miscmaths -lfsl-utils \
           -lfsl-newran -lfsl-NewNifti -lfsl-znz -lfsl-cprob

all:  ${SOFILES}

libfsl-dpm.so: dpm_gibbs.o hdpm_gibbs.o dpmOptions.o dpm_utils.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}
