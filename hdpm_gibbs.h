#if !defined(hdpm_gibbs_h)
#define hdpm_gibbs_h

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <ostream>

#include "newran/newran.h"
#include "miscmaths/miscprob.h"
#include "miscmaths/miscmaths.h"

#include "gibbs.h"
#include "dpm_utils.h"


namespace DPM {

class HDPM_GibbsSampler : public GibbsSampler
{
 protected:
  int                                   m_J;
  int                                   m_d;
  std::vector<int>                      m_n;
  int                                   m_ntot;
  int                                   m_k;

  float                                 m_alpha;
  float                                 m_gamma;

  float                                 m_a0;
  float                                 m_b0;

  std::vector< std::vector< std::pair<float,int> > >   randindex;

  std::vector< std::vector<int> >                 m_nperclass;     // number of data points per class in each group
  std::vector< int >                         m_ntotperclass;  // total number of data points per class
  std::vector< int >                         m_nsubjperclass; // number of subjects per class
  std::vector< int >                         m_nclasspersubj; // number of non-empty classes per subject
  std::vector< std::vector<int> >                 m_z;             // data label in each group
  std::vector< std::vector<int> >                 m_w;             // mapping from local clusters to global clusters


  // Spatial priors
  bool                     m_spat;
  std::vector<NEWMAT::Matrix>           m_adj; // adjacency matrix
  std::vector<float>            m_logspatterm;
  float                    m_spcparam;

  // dimensionality reduction
  bool m_reduced;
  NEWMAT::Matrix m_projector;


  // samples
  std::vector<float>            m_sample_alpha;
  std::vector<int>              m_sample_k;
  std::vector<double>           m_sample_likelihood;
  double                   m_likelihood;
  int                      m_nsamples;

  std::vector<NEWMAT::ColumnVector> m_map_z;
  std::vector<NEWMAT::Matrix> m_map_fuzzy_z;

  bool                      infinite;


  const std::vector<NEWMAT::Matrix>&                 m_data;

 public:
 HDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data):m_data(data){
    m_J = (int) m_data.size();
    m_d = data[0].Ncols();
    m_z.resize(m_J);
    m_w.resize(m_J);
    m_nperclass.resize(m_J);


    m_nsamples = (int)floor( m_njumps / m_sampleevery );

    m_sample_alpha.resize(m_nsamples);
    m_sample_k.resize(m_nsamples);
    m_sample_likelihood.resize(m_nsamples);


    m_ntot=0;
    for(int j=0;j<m_J;j++){
      m_n.push_back(data[j].Nrows());
      m_ntot += m_n[j];
      m_z[j].resize(m_n[j]);

      if(data[j].Ncols() != m_d)
	std::cerr << "One of the classes have different dimensionality" << std::endl;
    }

    NEWRAN::Random::Set(rand() / float(RAND_MAX));
    m_spat = false;

    infinite=true;

    m_reduced=false;
    m_projector = NEWMAT::IdentityMatrix(m_d);

  }
 HDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data,const int jj,const int bb,const int ss):
  GibbsSampler(jj,bb,ss),m_data(data){
    m_J = (int) m_data.size();
    m_d = data[0].Ncols();
    m_z.resize(m_J);
    m_w.resize(m_J);
    m_nperclass.resize(m_J);


    m_nsamples = (int)floor( m_njumps / m_sampleevery );

    m_sample_alpha.resize(m_nsamples);
    m_sample_k.resize(m_nsamples);
    m_sample_likelihood.resize(m_nsamples);


    m_ntot=0;
    for(int j=0;j<m_J;j++){
      m_n.push_back(data[j].Nrows());
      m_ntot += m_n[j];
      m_z[j].resize(m_n[j]);

      if(data[j].Ncols() != m_d){
	std::cerr << "One of the classes have different dimensionality" << std::endl;
	exit(0);
      }
    }

    NEWRAN::Random::Set(rand() / float(RAND_MAX));
    m_spat=false;

    m_reduced=false;
    m_projector = NEWMAT::IdentityMatrix(m_d);


  }

  void init(const int& k=5,const std::string& scheme="kmeans");
  virtual void init_own_params() = 0;
  virtual void init_oneperdata() = 0;
  virtual void init_onebigclass() = 0;
  virtual void init_kmeans(const int) = 0;
  virtual void init_random(const int) = 0;
  virtual void init_max() = 0;

  void set_infinite(const bool x){infinite=x;}

  void sample_parameters();
  void sample_hyperparameters(const bool);

  void sample_z();
  void sample_zj(const int);
  void sample_w();
  void sample_wj(const int);

  void mode_z();
  void mode_fuzzy_z();
  void mode_zj(const int);
  void mode_fuzzy_zj(const int);

  std::vector<NEWMAT::ColumnVector> get_map_z()const{return m_map_z;}
  NEWMAT::ColumnVector get_map_z(int j)const{return m_map_z[j];}
  std::vector<NEWMAT::Matrix> get_map_fuzzy_z()const{return m_map_fuzzy_z;}

  void sample_alpha();
  void sample_gamma();


  virtual void sample_own_params() = 0;
  virtual void sample_own_hypers() = 0;

  virtual void add_class(const NEWMAT::Matrix&) = 0;
  virtual void add_class(const NEWMAT::Matrix&,const int) = 0;
  virtual void delete_empty_classes() = 0;


  virtual double marglik(const NEWMAT::ColumnVector&,const int,const int) = 0;
  virtual double marglik(const NEWMAT::ColumnVector&,const int) = 0;
  virtual double margint(const NEWMAT::ColumnVector&) = 0;

 // spatial stuff
  void add_spatial_prior(const std::string& adjfile){
    m_spat=true;
    m_adj.clear();
    std::ifstream fs(adjfile.c_str());
    std::string tmp;
    NEWMAT::Matrix tmpadj;
    if(fs){
      fs>>tmp;
      do{
        tmpadj=MISCMATHS::read_ascii_matrix(tmp);
	m_adj.push_back(tmpadj);
	fs>>tmp;
      }while(!fs.eof());
    }
    else{
      std::cerr<<adjfile<<" does not exist"<<std::endl;
      exit(0);
    }
    if((int)m_adj.size()!=m_J){
      std::cerr<<"Adjacency file incompatible with data file"<<std::endl;
      exit(0);
    }
  }
  void add_spatial_prior(const std::vector<NEWMAT::Matrix>& adj){
    m_spat=true;
    m_adj = adj;
    if((int)m_adj.size()!=m_J){
      std::cerr<<"Adjacency file incompatible with data file"<<std::endl;
      exit(0);
    }
  }
  void init_spatial();
  inline float get_spcparam()const{return m_spcparam;}
  void set_spcparam(const float& x){m_spcparam=x;}

  void set_projector(const NEWMAT::Matrix& P){
    m_reduced=true;
    m_projector=P;
  }
  NEWMAT::Matrix get_projector()const{return m_projector;}

  double calc_loglikelihood();
  virtual double calc_logprior() = 0;
  double calc_energy();


  std::vector<NEWMAT::ColumnVector> get_dataindex();
  NEWMAT::ReturnMatrix get_dataindex(int);
  NEWMAT::ReturnMatrix calc_mldataindex(int);
  NEWMAT::ReturnMatrix calc_grpdataindex();


  std::vector<NEWMAT::ColumnVector>    calc_posterior_means();
  std::vector<NEWMAT::SymmetricMatrix> calc_posterior_variances();
  std::vector<float>                   calc_posterior_proportions();


  int get_numclass()const{return m_k;}
  virtual void record_own_params(const int) = 0;
  void record(const int samp){
    //cout<<"record sample "<<samp<<endl;
    m_sample_likelihood[samp] = m_likelihood;
    m_sample_k[samp]          = m_k;
    m_sample_alpha[samp]      = m_alpha;

    record_own_params(samp);
  }


  virtual void print_own_hyper() = 0;
  void print(std::ostream& os);

  void save(const std::string& filename){
    std::string zfile;
    NEWMAT::ColumnVector index;
    std::cout<<"saving"<<std::endl;
    for(int j=0;j<m_J;j++){
      index = calc_mldataindex(j);
      zfile  = filename + ".z"+MISCMATHS::num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=1;i<=m_n[j];i++){
	of_z << index(i) << std::endl;
      }
    }



  }
  void save_current(const std::string& filename){
    for(int j=0;j<m_J;j++){
      std::string zfile  = filename + ".z"+MISCMATHS::num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=0;i<m_n[j];i++){
	of_z << m_z[j][i] << std::endl;
      }
    }

  }


};

class GWHDPM_GibbsSampler : public HDPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,GWHDPM_GibbsSampler& g);

 protected:
  std::vector<GaussianWishart>               m_gw;
  GaussianWishart                       m_gw0;

  NEWMAT::ColumnVector                          m_m0;
  NEWMAT::SymmetricMatrix                       m_S0;
  NEWMAT::SymmetricMatrix                       m_N0;
  int                                   m_n0;

  double                                m_margintbase;

  // records
  std::vector< std::vector<NEWMAT::ColumnVector> >    m_sample_means;
  std::vector< std::vector<NEWMAT::SymmetricMatrix> > m_sample_variances;
  std::vector< std::vector<float> >           m_sample_proportions;

  // MAP estimations
  std::vector< NEWMAT::ColumnVector > m_map_means;
  std::vector< NEWMAT::SymmetricMatrix > m_map_variances;
  std::vector< float > m_map_proportions;



 public:
 GWHDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data,const int j,const int b,const int s):
  HDPM_GibbsSampler(data,j,b,s){}
  GWHDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data):
  HDPM_GibbsSampler(data){}
  ~GWHDPM_GibbsSampler(){}

  void sample_gw();
  void sample_gw0();

  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_max();
  void init_kmeans(const int);
  void init_random(const int);


  void sample_own_params();
  void sample_own_hypers();

  void mode_own_params();
  void mode_own_hypers();

  void add_class(const NEWMAT::Matrix& m,const int j){add_class(m);}
  void add_class(const NEWMAT::Matrix&);
  void delete_empty_classes();

  std::vector<NEWMAT::ColumnVector>    get_means()const;
  std::vector<NEWMAT::SymmetricMatrix> get_variances()const;
  std::vector<float>           get_proportions()const;
  std::vector<NEWMAT::ColumnVector>    calc_posterior_means();
  std::vector<NEWMAT::SymmetricMatrix> calc_posterior_variances();
  std::vector<float>           calc_posterior_proportions();


  void calc_posterior_stats();
  std::vector<NEWMAT::ColumnVector> get_map_means()const{return m_map_means;}
  std::vector<NEWMAT::SymmetricMatrix> get_map_variances()const{return m_map_variances;}
  std::vector<float> get_map_proportions()const{return m_map_proportions;}


  NEWMAT::ReturnMatrix calc_fstat();
  NEWMAT::ReturnMatrix calc_fstat(const std::vector<NEWMAT::ColumnVector>&,const std::vector<NEWMAT::SymmetricMatrix>&);
  float calc_fstat(const int& dim);
  float calc_meandiff(const int& dim);
  void project_classes(std::vector<NEWMAT::ColumnVector>&,std::vector<NEWMAT::SymmetricMatrix>&)const;
  NEWMAT::ReturnMatrix calc_zmap(const int& j);


  double marglik(const NEWMAT::ColumnVector& d,const int j,const int k){
    return marglik(d,k);
  }
  double marglik(const NEWMAT::ColumnVector&,const int);
  double margint(const NEWMAT::ColumnVector&);
  double calc_logprior(){return 0;}

  void print_own_hyper(){}
  void record_own_params(const int samp){
    m_sample_means[samp]       = get_means();
    m_sample_variances[samp]   = get_variances();
    m_sample_proportions[samp] = get_proportions();
  }

  void save(const std::string& filename){
    NEWMAT::ColumnVector index;
    std::string zfile;
    for(int j=0;j<m_J;j++){
      index = get_dataindex(j);
      zfile  = filename + ".z"+MISCMATHS::num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=1;i<=m_n[j];i++){
	of_z << index(i) << std::endl;
      }
    }

    std::vector<NEWMAT::ColumnVector>    postmeans   = calc_posterior_means();
    std::vector<NEWMAT::SymmetricMatrix> postvars    = calc_posterior_variances();
    std::vector<float>                   postprop    = calc_posterior_proportions();
    for(int k=0;k<m_k;k++){
      m_gw[k].set_smu(postmeans[k]);
      m_gw[k].set_ssigma(postvars[k]);
    }
    //map_z();
    for(int j=0;j<m_J;j++){
      index = get_dataindex(j);
      zfile  = filename + ".zmap"+MISCMATHS::num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=1;i<=m_n[j];i++){
	of_z << index(i) << std::endl;
      }
    }

    std::string mfile  = filename + ".means";
    std::string vfile  = filename + ".variances";
    std::ofstream of_m(mfile.c_str());
    std::ofstream of_v(vfile.c_str());
    for(int k=0;k<m_k;k++){
      of_m << m_gw[k].get_smu().t();
      of_v << m_gw[k].get_ssigma().t();
    }

    // group stuff
    index=calc_grpdataindex();
    zfile  = filename + ".zgrp";
    std::ofstream of_z(zfile.c_str());
    for(int i=1;i<=m_n[0];i++)
      of_z << index(i) << std::endl;




  }



};


class EXPHDPM_GibbsSampler : public HDPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,EXPHDPM_GibbsSampler& g);

 protected:
  std::vector<MDGamma>                  m_mdg;
  MDGamma                               m_mdg0;

  double                                m_margintbase;
  double                                m_maxloglik;

 public:
 EXPHDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data,const int j,const int b,const int s):
  HDPM_GibbsSampler(data,j,b,s){}
 EXPHDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& data):
  HDPM_GibbsSampler(data){}
  ~EXPHDPM_GibbsSampler(){}

  void sample_gw();
  void sample_gw0();

  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_max();
  void init_kmeans(const int);
  void init_random(const int);


  void sample_own_params();
  void sample_own_hypers();

  void add_class(const NEWMAT::Matrix& m,const int j){add_class(m);}
  void add_class(const NEWMAT::Matrix&);
  void delete_empty_classes();


  double marglik(const NEWMAT::ColumnVector& d,const int j,const int k){
    return marglik(d,k);
  }
  double marglik(const NEWMAT::ColumnVector&,const int);
  double margint(const NEWMAT::ColumnVector&);

  double calc_logprior();
  void record_own_params(const int samp){}
  void print_own_hyper(){}



};

}

#endif
