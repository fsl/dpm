#if !defined(redpm_gibbs_h)
#define redpm_gibbs_h

#include "armawrap/newmat.h"

#include "gibbs.h"
#include "dpm_utils.h"
#include "hdpm_gibbs.h"

// Implementation of the random effects in Hierarchical DPM
class REDPM_GibbsSampler : public HDPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,REDPM_GibbsSampler& g);

 protected:
  std::vector< std::vector<MDGamma> >   m_mdg;    // Local class parameters
  std::vector<MDGamma>                  m_mdg0;   // Hyperprior on global parameters


  double m_margintbase;
  double m_maxloglik;


 public:
 REDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& d,const int j,const int b,const int s):
  HDPM_GibbsSampler(d,j,b,s){}
 REDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& d):
  HDPM_GibbsSampler(d){}

  ~REDPM_GibbsSampler(){}

  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_max();
  void init_kmeans(const int);
  void init_random(const int);

  void sample_own_params();
  void sample_own_hypers();

  void add_class(const NEWMAT::Matrix&){std::cout << "CAREFUL MAN!!!!!" << std::endl;}
  void add_class(const NEWMAT::Matrix&,const int);
  void delete_empty_classes();

  double marglik(const NEWMAT::ColumnVector&,const int){std::cout << "dont wanna use this!!!!"<<std::endl;return 0;}
  double marglik(const NEWMAT::ColumnVector&,const int,const int);
  double margint(const NEWMAT::ColumnVector&);

  double calc_logprior(){return 0;}

  void print_own_hyper(){
/*     for(int j=0;j<m_J;j++) */
/*       for(int k=0;k<m_k;k++){ */
/* 	cout << "subject " << j << ", cluster " << k << endl; */
/* 	cout << m_mdg[j][k]; */
/*       } */
  }

  void save(const std::string& filename){
    std::cout << "saving" << std::endl;
    for(int j=0;j<m_J;j++){
      NEWMAT::ColumnVector index;
      index = calc_mldataindex(j);
      std::string zfile  = filename + ".z"+num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=1;i<=m_n[j];i++){
	of_z << index(i) << endl;
      }
    }

    NEWMAT::ColumnVector index;
    index = calc_grpdataindex();
    std::string zfile  = filename + ".zgroup";
    std::ofstream of_z(zfile.c_str());
    for(int i=1;i<=m_n[0];i++){
      of_z << index(i) << endl;
    }



  }

};
// Same with Gaussian Likelihood
class RGDPM_GibbsSampler : public HDPM_GibbsSampler
{
 private:
  friend std::ostream& operator << (std::ostream& o,RGDPM_GibbsSampler& g);

 protected:
  std::vector< std::vector<GaussianWishart> >   m_gw;    // Local class parameters
  std::vector<GaussianWishart>             m_gw0;   // Hyperprior on global parameters

  NEWMAT::ColumnVector                          m_m0;
  NEWMAT::SymmetricMatrix                       m_S0;
  NEWMAT::SymmetricMatrix                       m_N0;
  int                                   m_n0;


  double m_margintbase;



 public:
 RGDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& d,const int j,const int b,const int s):
  HDPM_GibbsSampler(d,j,b,s){}
 RGDPM_GibbsSampler(const std::vector<NEWMAT::Matrix>& d):
  HDPM_GibbsSampler(d){}

  ~RGDPM_GibbsSampler(){}

  void init_own_params();
  void init_oneperdata();
  void init_onebigclass();
  void init_max();
  void init_kmeans(const int);
  void init_random(const int);

  void sample_own_params();
  void sample_own_hypers();

  void add_class(const NEWMAT::Matrix&){cout << "CAREFUL MAN!!!!!" << endl;}
  void add_class(const NEWMAT::Matrix&,const int);
  void delete_empty_classes();

  double marglik(const NEWMAT::ColumnVector&,const int){cout << "dont wanna use this!!!!"<<endl;return 0;}
  double marglik(const NEWMAT::ColumnVector&,const int,const int);
  double margint(const NEWMAT::ColumnVector&);

  double calc_logprior(){return 0;}

  void print_own_hyper(){
  }

  void save(const std::string& filename){
    std::cout << "saving" << std::endl;
    for(int j=0;j<m_J;j++){
      NEWMAT::ColumnVector index;
      index = calc_mldataindex(j);
      std::string zfile  = filename + ".z"+num2str(j);
      std::ofstream of_z(zfile.c_str());
      for(int i=1;i<=m_n[j];i++){
	of_z << index(i) << endl;
      }
    }

    NEWMAT::ColumnVector index;
    index = calc_grpdataindex();
    std::string zfile  = filename + ".zgroup";
    std::ofstream of_z(zfile.c_str());
    for(int i=1;i<=m_n[0];i++){
      of_z << index(i) << endl;
    }



  }

};

#endif
