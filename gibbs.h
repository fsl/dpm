/*  Copyright (C) 2007 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(_GIBBS_H)
#define _GIBBS_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#define DEFNUMBURN     1000
#define DEFNUMJUMPS    1000
#define DEFSAMPLEEVERY 10


class GibbsSampler
{
 protected:

  int m_njumps;
  int m_burnin;
  int m_sampleevery;

 public:
 GibbsSampler():m_njumps(DEFNUMJUMPS),m_burnin(DEFNUMBURN),m_sampleevery(DEFSAMPLEEVERY){}
  GibbsSampler(int njumps,int burnin,int sampleevery):
    m_njumps(njumps),m_burnin(burnin),m_sampleevery(sampleevery){}
  virtual ~GibbsSampler(){}

  virtual void record(const int) = 0;
  virtual void sample_parameters() = 0;
  virtual void sample_hyperparameters(const bool b=false) = 0;

  void  run(){

    int recordcount=0;
    int samp = 0;

    // burnin period (no sampling)
    //cout<<"burnin"<<endl;
    for(int i=0;i<m_burnin;i++){
      //cout<<"-----------"<<endl;
      sample_parameters();
      sample_hyperparameters(false);

      //recordcount++;

      //if(recordcount==m_sampleevery){
	//cout<<"record"<<endl;
	//record(samp);samp++;
	//recordcount=0;
      //}
    }

    // m_numiter=2;

    // after burnin, sample ervery "sampleevery"
    //    cout<<"gibbs"<<endl;

    for(int i=0;i<m_njumps;i++){
      sample_parameters();
      sample_hyperparameters(true);

      //print();

      recordcount++;

      if(recordcount==m_sampleevery){
	//cout<<"record"<<endl;
	record(samp);samp++;
	recordcount=0;
      }
    }


  }

};



#endif
