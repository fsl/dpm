#include "dpm_gibbs.h"

using namespace std;
using namespace NEWMAT;
using namespace NEWRAN;
using namespace MISCMATHS;

namespace DPM {

std::ostream& operator << (ostream& o,DPM_GibbsSampler& g){
  g.print(o);
  return o;
}
std::ostream& operator << (ostream& o,GWDPM_GibbsSampler& g){
  g.print(o);
  return o;
}
void DPM_GibbsSampler::init(const int& numclass,const string& scheme){
  // alpha prior
  double mean_alpha = 1;
  double var_alpha  = 10;

  m_a0       = mean_alpha*mean_alpha/var_alpha;
  m_b0       = mean_alpha/var_alpha;

  // initialise all other parameters
  m_alpha    = mean_alpha;
  m_k        = numclass;

  init_own_params();

  // class parameters

  if(scheme == "oneperdata")
    init_oneperdata();
  else if (scheme == "one")
    init_onebigclass();
  else if (scheme == "kmeans")
    init_kmeans(numclass);
  else if (scheme == "max")
    init_max();
  else if (scheme == "random")
    init_random(numclass);
  else{
    cerr << "Unknown Initialisation Scheme" << endl;
    exit(0);
  }

  init_spatial();

  // randomised index for loop over data items
  randindex.resize(m_n);

  //cout << "JUST INITIALISED" <<endl;
  //cout << *this;
  //exit(0);
}
void DPM_GibbsSampler::init_spatial(){
  m_logspatterm = 0.0;
  //m_spcparam = param;

  if(!m_spat)return;

  for(int i=1;i<=m_n;i++){
    for(int j=1;j<=m_adj.Ncols();j++){
      if(m_adj(i,j)>0)
	m_logspatterm += ((m_z[i-1]!=m_z[(int)m_adj(i,j)-1])?1:0);
    }
  }
  //  OUT(m_logspatterm);
  //  exit(0);

}
void DPM_GibbsSampler::sample_parameters(){
  cout << *this;

  sample_z();
  sample_own_params();

  //print_own_hyper();
}
void DPM_GibbsSampler::sample_hyperparameters(const bool isburned){

  sample_own_hypers();
  sample_alpha(isburned);

}
// sample indicator variables
void DPM_GibbsSampler::sample_z(){
  ColumnVector datapoint(m_d);
  randomise(randindex);

  // if finite gaussian mixture, do not add new classes
  float extra_finite1 = infinite ? 0.0 : m_alpha/float(m_k);
  float extra_finite2 = infinite ? 1.0 : 0.0;

  vector< pair<float,int> >::iterator iter;
  for(iter=randindex.begin(); iter!=randindex.end(); ++iter){
    ColumnVector cumsum(m_k+1);
    //    ColumnVector w(m_k+1);
    ColumnVector spcterm(m_k+1);
    spcterm = 0;

    datapoint=m_data.Row((*iter).second+1).t();
    int oldz=m_z[(*iter).second],newz=oldz;
    m_classnd[oldz] -= 1;

    // compute class weights
    double sum=0.0;
    for(int k=0;k<m_k;k++){
      if(m_spat && k!=oldz){
	for(int neigh=1;neigh<=m_adj.Ncols();neigh++){
	  if(m_adj((*iter).second+1,neigh)<=0)continue;
	  if(m_z[m_adj((*iter).second+1,neigh)-1]==oldz)
	    spcterm(k+1) += 2;
	  else{
	    if(m_z[m_adj((*iter).second+1,neigh)-1]==newz)
	      spcterm(k+1) -= 2;
	  }
	}
      }

      sum += float(m_classnd[k]+extra_finite1)*exp(marglik(datapoint,k)-spcterm(k+1)*m_spcparam/4);
      //w(k+1) = float(m_classnd[k]+extra_finite1)*exp(marglik(datapoint,k)-spcterm(k+1)*m_spcparam/4);
      cumsum(k+1) = sum;
    }
    if(m_spat)
      for(int neigh=1;neigh<=m_adj.Ncols();neigh++){
	if(m_adj((*iter).second+1,neigh)<=0)continue;
	if(m_z[m_adj((*iter).second+1,neigh)-1]==oldz)
	  spcterm(m_k+1) += 2;
      }

    sum += m_alpha*exp(margint(datapoint)-spcterm(m_k+1)*m_spcparam/4) * extra_finite2;
    cumsum(m_k+1) = sum;
    //w(m_k+1)=m_alpha*exp(margint(datapoint)-spcterm(m_k+1)*m_spcparam/4) * extra_finite2;
    //OUT(w.t());

    // sample z using the weights
    float U=rand()/float(RAND_MAX);
    U *= sum;
    for(int k=1;k<=m_k+1;k++){
      if(U<cumsum(k)){
	newz=k-1;
	break;
      }
    }
    m_z[(*iter).second] = newz;

    m_logspatterm += spcterm(newz+1); // update spatial prior
    //cout << "new spcterm" << endl;
    //OUT(m_logspatterm);
    //OUT(m_spat);

    if( newz >= m_k ){ // add a new class
      m_k++;
      m_classnd.push_back(1);
      add_class(datapoint.t());

    }
    else{
      m_classnd[newz] += 1;
    }

  }// end loop over data points

  if(infinite){
    delete_empty_classes();
  }
  //  exit(0);

}

// sample indicator variables
void DPM_GibbsSampler::mode_z(){
  m_map_z.ReSize(m_n);

  ColumnVector datapoint(m_d);
  randomise(randindex);
  // if finite gaussian mixture, do not add new classes
  float extra_finite1 = infinite ? 0.0 : m_alpha/float(m_k);
  vector< pair<float,int> >::iterator iter;
  for(iter=randindex.begin(); iter!=randindex.end(); ++iter){
    ColumnVector w(m_k);
    ColumnVector spcterm(m_k);
    spcterm = 0;
    datapoint=m_data.Row((*iter).second+1).t();
    int oldz=m_z[(*iter).second],newz=oldz;
    m_classnd[oldz] -= 1;
    // compute class weights
    for(int k=0;k<m_k;k++){
      if(m_spat && k!=oldz){
	for(int neigh=1;neigh<=m_adj.Ncols();neigh++){
	  if(m_z[m_adj((*iter).second+1,neigh)-1]==oldz)
	    spcterm(k+1) += 2;
	  else{
	    if(m_z[m_adj((*iter).second+1,neigh)-1]==newz)
	      spcterm(k+1) -= 2;
	  }
	}
      }
      w(k+1) = exp(log(m_classnd[k]+extra_finite1)+marglik(datapoint,k)-spcterm(k+1)*m_spcparam/4);
    }
    w.Maximum1(newz);
    newz--;
    m_z[(*iter).second] = newz;
    m_classnd[newz] += 1;//???

  }// end loop over data points
  delete_empty_classes();

  for(int i=1;i<=m_n;i++){
    m_map_z(i) = m_z[i-1];
  }

}

  void DPM_GibbsSampler::mode_fuzzy_z(){
    m_map_fuzzy_z.ReSize(m_n,m_k);

    ColumnVector datapoint(m_d);

    float extra_finite1 = infinite ? 0.0 : m_alpha/float(m_k);
    vector< pair<float,int> >::iterator iter;
    for(int i=1;i<=m_n;i++){
      ColumnVector spcterm(m_k);
      spcterm = 0;
      datapoint=m_data.Row(i).t();
      int oldz=m_z[i-1],newz=oldz;
      m_classnd[oldz] -= 1;
      // compute class weights
      float wsum=0;
      for(int k=0;k<m_k;k++){
	if(m_spat && k!=oldz){
	  for(int neigh=1;neigh<=m_adj.Ncols();neigh++){
	    if(m_z[m_adj(i,neigh)-1]==oldz)
	      spcterm(k+1) += 2;
	    else{
	      if(m_z[m_adj(i,neigh)-1]==newz)
		spcterm(k+1) -= 2;
	    }
	  }
	}
	m_map_fuzzy_z(i,k+1) = exp(log(m_classnd[k]+extra_finite1)+marglik(datapoint,k)-spcterm(k+1)*m_spcparam/4);
	wsum += m_map_fuzzy_z(i,k+1);
      }
      m_classnd[oldz] += 1;//???

      // normalise
      if(wsum>0)
	m_map_fuzzy_z.Row(i) /= wsum;

    }// end loop over data points


  }

// sample from alpha using additional variable eta
void DPM_GibbsSampler::sample_alpha(const bool isburned){
  float eta,prop;
  float ak=m_a0+m_k-1,bn;


  Gamma G1(ak+1);       //G1.Set(rand()/float(RAND_MAX));
  Gamma G2(ak);         //G2.Set(rand()/float(RAND_MAX));
  Gamma B1(m_alpha+1);  //B1.Set(rand()/float(RAND_MAX));
  Gamma B2(m_n);        //B2.Set(rand()/float(RAND_MAX));

  eta  = B1.Next();
  eta /= (eta+B2.Next());

//   if(isburned)
//     cout<<"BURNED!!!!"<<endl;
//   else
//     cout<<"NOT YET!!!"<<endl;

  float bb=m_b0;//isburned?1.0E8:m_b0;
  bn   = bb-std::log(eta);

  prop=ak/(ak+m_n*bn);
  m_alpha=(prop*G1.Next()+(1-prop)*G2.Next())/bn;

}


ReturnMatrix DPM_GibbsSampler::get_dataindex(){
    ColumnVector index(m_n);
    for(int i=0;i<m_n;i++)
      index(i+1) = m_z[i];
    index.Release();
    return index;
}
ReturnMatrix DPM_GibbsSampler::calc_mldataindex(){
  ColumnVector index(m_n);
  double lik,tmplik;

  for(int i=1;i<=m_n;i++){
    lik=-1.0;tmplik=0;index(i) = 0;
    for(int k=0;k<m_k;k++){
      tmplik = float(m_classnd[k]-1)*exp(marglik(m_data.Row(i).t(),k));
      //tmplik = marglik(m_data.Row(i).t(),k);

      if(tmplik>lik || k==0){
	lik = tmplik;
	index(i) = k;
      }
    }
  }
  index.Release();
  return index;
}
ReturnMatrix GWDPM_GibbsSampler::calc_fstat(){
  Matrix fstat;

  if(m_reduced){
    vector<ColumnVector> mu;
    vector<SymmetricMatrix> sig;
    project_classes(mu,sig);
    fstat=calc_fstat(mu,sig);
  }
  else{
    fstat.ReSize(m_d,2);
    for(int j=1;j<=m_d;j++){
      fstat(j,1) = calc_meandiff(j);
      fstat(j,2) = calc_fstat(j);
    }
  }

  fstat.Release();
  return(fstat);
}
ReturnMatrix GWDPM_GibbsSampler::calc_fstat(const vector<ColumnVector>& means,const vector<SymmetricMatrix>& vars){
  Matrix fstat;
  float m,v,ss;

  fstat.ReSize(means[0].Nrows(),2);
  for(int j=1;j<=means[0].Nrows();j++){
    m=0.0;v=0.0,ss=0.0;
    for(int k=0;k<m_k;k++){
      ss += means[k](j)*means[k](j);
      m  += means[k](j);
      v  += vars[k](j,j);
    }
    fstat(j,1) = (ss - m*m/float(m_k));
    fstat(j,2) = (ss - m*m/float(m_k))/(float(m_k)*v);

  }
  fstat.Release();
  return(fstat);
}
float GWDPM_GibbsSampler::calc_fstat(const int& dim){
  float f;
  float m,v,ss;
  m=0.0;v=0.0,ss=0.0;

  for(int k=0;k<m_k;k++){
    ss += m_gw[k].get_smu()(dim)*m_gw[k].get_smu()(dim);
    m  += m_gw[k].get_smu()(dim);
    v  += m_gw[k].get_ssigma()(dim,dim);
  }
  f = (ss - m*m/float(m_k))/(float(m_k)*v);

  return(f);
}
float GWDPM_GibbsSampler::calc_meandiff(const int& dim){
  float f;
  float m,ss;
  m=0.0;ss=0.0;

  for(int k=0;k<m_k;k++){
    ss += m_gw[k].get_smu()(dim)*m_gw[k].get_smu()(dim);
    m  += m_gw[k].get_smu()(dim);
  }
  f = (ss - m*m/float(m_k));

  return(f);
}
  void GWDPM_GibbsSampler::project_classes(vector<ColumnVector>& mu,vector<SymmetricMatrix>& sig)const{

    // if(m_projector.Nrows()==m_d){
//       m_projector = m_projector.t();
//     }

    mu.clear();sig.clear();
    ColumnVector      X(m_projector.Nrows());
    SymmetricMatrix   S(m_projector.Nrows());

    for(int k=0;k<m_k;k++){
      X = m_projector*m_gw[k].get_smu();
      S << m_projector*m_gw[k].get_ssigma()*m_projector.t();

      mu.push_back(X);
      sig.push_back(S);
    }

  }
///////////////////////////////////////////////////////////////////////////////
// GaussianWishart Stuff
void GWDPM_GibbsSampler::init_own_params(){
  m_S0       << 1.0E3*IdentityMatrix(m_d);//
  //m_N0       << cov(m_data);
  m_N0       << IdentityMatrix(m_d);///(m_n-m_d-1);
  m_m0       = mean(m_data,1).t();
  m_n0       = 1.0;

  // class hyper parameters
  float kappa0   = 1.0;
  int nu0        = m_d;
  SymmetricMatrix Nu0(m_d);
  Nu0 << m_n0*m_N0;//.01*m_d*Identity(m_d);//cov(m_data);//*(m_nu0-m_d-1);
  ColumnVector mu0(m_d);
  mu0 = m_m0;

  m_gw0      = GaussianWishart(mu0,Nu0,nu0,kappa0);

  // calculate part of the marginalisation over class mean/variance
  // this part doesn't change through the iterations
  m_margintbase = -float(m_d)/2*log(2*M_PI)
    -lgam(float(nu0+1-m_d)/2.0) + lgam(float(nu0+1)/2.0);


  //  m_margintbase += m_d/2*log(kappa0/(1+kappa0));


  m_lambdabase.ReSize(m_n);
  for(int i=1;i<=m_n;i++){
    m_lambdabase(i) = float(nu0+i)/2*log(2) + m_d*(m_d-1)/2*log(M_PI)+m_d/2*log(2*M_PI);
    for(int j=1;j<=m_d;j++)
      m_lambdabase(i) += lgam(float(nu0+i+1-j)/2);
  }


  // allocate some space for samples
  m_sample_means.resize(m_nsamples);
  m_sample_variances.resize(m_nsamples);
  m_sample_proportions.resize(m_nsamples);

}
// different initialisation schemes
void GWDPM_GibbsSampler::init_oneperdata(){
  m_k = m_n;
  m_z.clear();m_gw.clear();m_classnd.clear();
  // set parameters
  for(int i=1;i<=m_n;i++){
    GaussianWishart gw(m_d);
    gw.postupdate(m_data.SubMatrix(i,i,1,m_d),m_gw0);
    m_gw.push_back(gw);
    m_z.push_back(i-1);
    m_classnd.push_back(1);
  }
}
void GWDPM_GibbsSampler::init_onebigclass(){
  m_k = 1;
  m_z.clear();m_gw.clear();m_classnd.clear();
  GaussianWishart gw(m_d);
  gw.postupdate(m_data,m_gw0);
  m_gw.push_back(gw);
  for(int i=0;i<m_data.Nrows();i++)m_z.push_back(0);
  m_classnd.push_back(m_data.Nrows());
}
void GWDPM_GibbsSampler::init_max(){
  // initialise z with maximum connectivity
  m_z.clear();
  int indmax=0;
  for(int i=0;i<m_n;i++){
    int index;
    m_data.Row(i+1).Maximum1(index);
    m_z.push_back(index-1);
    indmax = index>indmax?index:indmax;
  }
  cout << indmax << endl;
  m_k = indmax;
  m_gw.clear();m_classnd.clear();
  for(int k=0;k<m_k;k++){
    GaussianWishart gw(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    if(dat.size()==0)cout<<"hooo"<<endl;
    gw.postupdate(dat,m_gw0);
    m_gw.push_back(gw);
    m_classnd.push_back((int)dat.size());
  }
  cout << "done." << endl;
}
void GWDPM_GibbsSampler::init_kmeans(const int k){
  m_k=k;
  m_gw.clear();m_classnd.clear();
  m_z.resize(m_n);


  //  do_kmeans(m_data,m_z,m_k,10);

  float mincc=-10,cc=0;
  vector<int> zz(m_n);int kk=k;
  for(int rep=1;rep<=1;rep++){
    //OUT(rep);
    cc=do_kmeans(m_data,zz,kk);
    if(mincc==-10 || cc<=mincc){
      mincc=cc;
      m_z = zz;
      m_k = kk;
    }
  }
  //cout << "done" << endl;

  for(int k=1;k<=m_k;k++){
    GaussianWishart gw(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    gw.postupdate(dat,m_gw0);
    m_gw.push_back(gw);
    m_classnd.push_back((int)dat.size());
  }
  delete_empty_classes();

}
void GWDPM_GibbsSampler::init_random(const int k){
  m_k=k;
  m_gw.clear();m_classnd.clear();
  m_z.resize(m_n);
  vector< pair<float,int> > rindex(m_n);
  randomise(rindex);
  vector<pair<float,int> >::iterator riter;
  int nn=0,cl=1,nperclass=(int)(float(m_n)/float(m_k));
  for(riter=rindex.begin();riter!=rindex.end();++riter){
    m_z[(*riter).second]=cl;
    nn++;
    if(nn>=nperclass && cl<m_k){
      nn=0;
      cl++;
    }
  }
  for(int k=1;k<=m_k;k++){
    GaussianWishart gw(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    gw.postupdate(dat,m_gw0);
    m_gw.push_back(gw);
    m_classnd.push_back((int)dat.size());
  }
}

void GWDPM_GibbsSampler::add_class(const Matrix& data){
  GaussianWishart gw(m_d);
  gw.postupdate(data,m_gw0);
  //gw.set_smu(mean(data,1).t());
  m_gw.push_back(gw);
}

void GWDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_classnd[k] == 0){
      for(int i=0;i<m_n;i++)
	if(m_z[i]>k)m_z[i]--;
      for(int kk=k;kk<m_k-1;kk++){
	m_classnd[kk]=m_classnd[kk+1];
	m_gw[kk]=m_gw[kk+1];
      }
      m_classnd.pop_back();
      m_gw.pop_back();
      m_k--;
    }
  }
}


void GWDPM_GibbsSampler::sample_own_params(){
  // update classes posteriors
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  // calculate likelihood
  m_likelihood = 0;
  for(int i=0;i<m_n;i++){
    data[ m_z[i] ].push_back(m_data.Row(i+1).t());
    m_likelihood += -marglik(m_data.Row(i+1).t(),m_z[i]);
  }

  for(int k=0;k<m_k;k++){
    if(data[k].size()>0)
      m_gw[k].postupdate(data[k],m_gw0);
  }
}
void GWDPM_GibbsSampler::mode_own_params(){
  // update classes posteriors
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  for(int i=0;i<m_n;i++)
    data[ m_z[i] ].push_back(m_data.Row(i+1).t());

  for(int k=0;k<m_k;k++){
    if(data[k].size()>0)
      m_gw[k].postmode(data[k],m_gw0);
  }
}


void GWDPM_GibbsSampler::sample_own_hypers(){
  SymmetricMatrix Nu0(m_d),AA(m_d),A(m_d),S(m_d);
  ColumnVector a(m_d),mu0(m_d);
  float B=0;

  A=0;a=0;AA=0;
  for(int k=0;k<m_k;k++){
    S = m_gw[k].get_ssigma().i();
    a += m_gw[k].get_kappa()*S*m_gw[k].get_smu();
    A << A+m_gw[k].get_kappa()*S;
    AA << AA+S;
    B += ((m_gw[k].get_smu()-m_gw0.get_mu()).t()*S*(m_gw[k].get_smu()-m_gw0.get_mu())).AsScalar();
  }
  S << AA+m_N0.i();
  A << (A+m_S0.i()).i();
  a = A*(a+m_S0.i()*m_m0);

  //  OUT(A);

  Nu0 = DPM::wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  mu0 = MISCMATHS::mvnrnd(a.t(),A).t();


  m_gw0.set_Nu(Nu0);
  m_gw0.set_mu(mu0);

  //float prior_shape = 1.0;
  //float prior_scale = 1.0;

  //Gamma G(prior_shape+float(m_k*m_d)/2); //G.Set(rand()/float(RAND_MAX));
  //m_gw0.set_kappa( (G.Next()/(prior_scale+B/2)));
  m_gw0.set_kappa(1.0);

  m_gw0.sample();

}

void GWDPM_GibbsSampler::mode_own_hypers(){
  SymmetricMatrix Nu0(m_d),AA(m_d),A(m_d),S(m_d);
  ColumnVector a(m_d),mu0(m_d);
  float B=0;

  A=0;a=0;AA=0;
  for(int k=0;k<m_k;k++){
    S = m_gw[k].get_ssigma().i();
    a += m_gw[k].get_kappa()*S*m_gw[k].get_smu();
    A << A+m_gw[k].get_kappa()*S;
    AA << AA+S;
    B += ((m_gw[k].get_smu()-m_gw0.get_mu()).t()*S*(m_gw[k].get_smu()-m_gw0.get_mu())).AsScalar();
  }
  S << AA+m_N0.i();
  A << (A+m_S0.i()).i();
  a = A*(a+m_S0.i()*m_m0);


  Nu0 = DPM::wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  mu0 = MISCMATHS::mvnrnd(a.t(),A).t();

  //Nu0 = (m_k+1)*m_gw0.get_dof() * S.i();//wishrnd(S.i(),(m_k+1)*m_gw0.get_dof());
  //mu0 = a;//mvnrnd(a.t(),A).t();


  m_gw0.set_Nu(Nu0);
  m_gw0.set_mu(mu0);

  float prior_shape = 1.0;
  float prior_scale = 1.0;

  Gamma G(prior_shape+float(m_k*m_d)/2); //G.Set(rand()/float(RAND_MAX));
  m_gw0.set_kappa( (G.Next()/(prior_scale+B/2)));
  //m_gw0.set_kappa(1.0);

  m_gw0.mode();

}

double GWDPM_GibbsSampler::marglik(const ColumnVector& data,const int k){
  double res=0.0;
  LogAndSign ld=(2*M_PI*m_gw[k].get_ssigma()).LogDeterminant();

  res -= 0.5*(ld.LogValue()
	      +((data-m_gw[k].get_smu()).t()
	      *m_gw[k].get_ssigma().i()
		*(data-m_gw[k].get_smu())).AsScalar());

  return res;
}
double GWDPM_GibbsSampler::margint(const ColumnVector& data,const int k){
  double res=0.0;
  ColumnVector mdat(m_d);
  SymmetricMatrix S(m_d),SS(m_d);

  mdat=0;S=0;SS=0;int n=0;

  for(int i=1;i<=m_n;i++){
    if(m_z[i-1]==k){
      SS << m_data.Row(i).t()*m_data.Row(i);
      S += SS;
      mdat += m_data.Row(i).t();
      n++;
    }
  }
  SS << S - n*mdat*mdat.t();
  SS << SS + m_gw0.get_kappa()*n/m_gw0.get_kappa()*(mdat-m_gw0.get_mu())*(mdat-m_gw0.get_mu()).t();

  //  OUT(m_lambdabase.t());
  res = m_lambdabase(n);

  LogAndSign ld;
  ld = (m_gw0.get_Nu()+SS).LogDeterminant();
  res -= float(m_gw0.get_dof()+n)/2*ld.LogValue() - m_d/2*log(m_gw0.get_kappa()+float(n));

  return res;

}
double GWDPM_GibbsSampler::margint(const ColumnVector& data){
  LogAndSign ld;
  double res=m_margintbase;
  float kk=m_gw0.get_kappa();

  res += float(m_d)/2*log(kk/(1+kk));

  ld = m_gw0.get_Nu().LogDeterminant();
  res += ld.LogValue()*float(m_gw0.get_dof())/2;

  SymmetricMatrix A(m_d);
  A << m_gw0.get_Nu()+kk/(1+kk)*(data-m_gw0.get_mu())*(data-m_gw0.get_mu()).t();
  ld = A.LogDeterminant();
  res -= ld.LogValue()*float(m_gw0.get_dof()+1)/2;

  return res;
}


double GWDPM_GibbsSampler::calc_bic(){
  vector<ColumnVector>    postmeans   = calc_posterior_means();
  vector<SymmetricMatrix> postvars    = calc_posterior_variances();
  vector<float>           postprop    = calc_posterior_proportions();

  for(int k=0;k<m_k;k++){
    m_gw[k].set_smu(postmeans[k]);
    m_gw[k].set_ssigma(postvars[k]);
  }
  mode_z();

  double loglik=0.0;double xx;
  for(int i=0;i<m_n;i++){
    xx=0;
    for(int k=0;k<m_k;k++)
      xx += postprop[k]*exp(marglik(m_data.Row(m_z[i]+1).t(),k));
    loglik += log(xx);
  }
  double penalty = double(m_k)*log(double(m_n));

  return -2*(loglik-penalty);
}

double GWDPM_GibbsSampler::calc_bic(const int& dim){
  double loglik=0.0;

  float MLmu  = mean(m_data.Column(dim)).AsScalar();
  float MLvar = var(m_data.Column(dim)).AsScalar();

  loglik += 0.5*double(m_n)*log(2*M_PI*MLvar);

  for(int i=1;i<=m_n;i++)
    loglik -= 0.5*(std::pow((float)m_data(i,dim)-MLmu,2)/MLvar);

  double penalty = log(double(m_n));

  cout << loglik << endl;
  cout << penalty << endl;

  return -2*(loglik-penalty);
}
double GWDPM_GibbsSampler::calc_rao_pvalue(const int& dim)const{
  ColumnVector d(m_k*(m_k-1)/2);
  Matrix distrib = read_ascii_matrix("/Users/saad/fsl/src/dpm/distrib");
  double pval=1.0;

  int kk=0;int m;
  for(int k1=0;k1<m_k;k1++){
    for(int k2=0;k2<k1;k2++){
      kk++;
      d(kk) = distfisher(m_gw[k1].get_smu()(dim),
			 m_gw[k1].get_ssigma()(dim,dim),
			 m_gw[k2].get_smu()(dim),
			 m_gw[k2].get_ssigma()(dim,dim));

      (distrib.Column(1)-d(kk)).MinimumAbsoluteValue1(m);
      pval *= distrib(m,2);
    }
  }

  //pval *= (m_k*(m_k-1)/2);
  return pval;
}

vector<ColumnVector> GWDPM_GibbsSampler::get_means()const{
  vector<ColumnVector> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(m_gw[k].get_smu());
  return ret;
}
vector<SymmetricMatrix> GWDPM_GibbsSampler::get_variances()const{
  vector<SymmetricMatrix> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(m_gw[k].get_ssigma());
  return ret;
}
vector<float> GWDPM_GibbsSampler::get_proportions()const{
  vector<float> ret;
  for(int k=0;k<m_k;k++)
    ret.push_back(float(m_classnd[k])/float(m_n));
  return ret;
}
vector<ColumnVector> GWDPM_GibbsSampler::calc_posterior_means(){
  vector<ColumnVector> ret;
  for(int k=0;k<m_k;k++){
    ColumnVector v(m_d);
    v=0.0;
    ret.push_back(v);
  }

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++){
    if((int)m_sample_means[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_means[samp][k];
    }
  }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}
vector<SymmetricMatrix> GWDPM_GibbsSampler::calc_posterior_variances(){
  vector<SymmetricMatrix> ret;
  for(int k=0;k<m_k;k++){
    SymmetricMatrix M(m_d);
    M=0.0;
    ret.push_back(M);
  }

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++)
    if((int)m_sample_variances[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_variances[samp][k];
    }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}
vector<float> GWDPM_GibbsSampler::calc_posterior_proportions(){
  vector<float> ret;
  for(int k=0;k<m_k;k++)ret.push_back(0.0);

  int nsamp=0;
  for(int samp=0;samp<m_nsamples;samp++)
    if((int)m_sample_proportions[samp].size() == m_k){
      nsamp++;
      for(int k=0;k<m_k;k++)
	ret[k] += m_sample_proportions[samp][k];
    }
  for(int k=0;k<m_k;k++)ret[k]/=nsamp;

  return ret;
}
  void GWDPM_GibbsSampler::calc_posterior_stats(){
    mode_own_hypers();
    mode_own_params();
    mode_z();
    mode_fuzzy_z();

    m_map_means       = get_means();
    m_map_variances   = get_variances();
    m_map_proportions = get_proportions();
  }



///////////////////////////////////////////////////////////////////////////////
// Exponential Stuff
void EXPDPM_GibbsSampler::init_own_params(){
  ColumnVector a(m_d);
  ColumnVector b(m_d);

  a=1.0;
  b=1.0;

  m_mdg0 = MDGamma(a,b);

  // calculate part of the marginalisation over class mean/variance
  // this part doesn't change through the iterations
  m_margintbase=0.0;
  for(int i=1;i<=m_d;i++)
    m_margintbase += log(m_mdg0.get_a(i));

  // scaling factor from maximum likelihood estimation
  // dealing wit numerical approximation
  m_maxloglik = 0;
  ColumnVector mlLambda = mean(m_data,1).t();

  double loglik;
  for (int i=1;i<=m_n;i++){
    loglik = 0.0;
    for(int j=1;j<=m_d;j++){
      loglik += -log(mlLambda(j)) - m_data(i,j)/mlLambda(j);
    }
    if(i==1)m_maxloglik=loglik;
    if( loglik > m_maxloglik) m_maxloglik = loglik;
  }

}
// different initialisation schemes
void EXPDPM_GibbsSampler::init_oneperdata(){
  m_k = m_n;
  m_z.clear();m_mdg.clear();m_classnd.clear();
  // set parameters
  for(int i=1;i<=m_n;i++){
    MDGamma mdg(m_d);
    mdg.postupdate(m_data.SubMatrix(i,i,1,m_d),m_mdg0);
    m_mdg.push_back(mdg);
    m_z.push_back(i-1);
    m_classnd.push_back(1);
  }
}
void EXPDPM_GibbsSampler::init_onebigclass(){
  m_k = 1;
  m_z.clear();m_mdg.clear();m_classnd.clear();
  MDGamma mdg(m_d);
  mdg.postupdate(m_data,m_mdg0);
  m_mdg.push_back(mdg);
  for(int i=0;i<m_data.Nrows();i++)m_z.push_back(0);
  m_classnd.push_back(m_data.Nrows());
}
void EXPDPM_GibbsSampler::init_max(){
  // initialise z with maximum connectivity
  m_z.clear();
  int indmax=0;
  for(int i=0;i<m_n;i++){
    int index;
    m_data.Row(i+1).Maximum1(index);
    m_z.push_back(index-1);
    indmax = index>indmax?index:indmax;
  }

  m_k = indmax;

  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    mdg.postupdate(dat,m_mdg0);
    m_mdg.push_back(mdg);
    m_classnd.push_back((int)dat.size());
  }

}
void EXPDPM_GibbsSampler::init_kmeans(const int k){
  m_k=k;
  m_mdg.clear();m_classnd.clear();
  m_z.resize(m_n);
  do_kmeans(m_data,m_z,m_k);

  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    mdg.postupdate(dat,m_mdg0);
    m_mdg.push_back(mdg);
    m_classnd.push_back((int)dat.size());
  }
}
void EXPDPM_GibbsSampler::init_random(const int k){
  m_k=k;
  m_mdg.clear();m_classnd.clear();
  m_z.resize(m_n);
  vector< pair<float,int> > rindex(m_n);
  randomise(rindex);
  vector<pair<float,int> >::iterator riter;
  int nn=0,cl=1,nperclass=(int)(float(m_n)/float(m_k));
  for(riter=rindex.begin();riter!=rindex.end();++riter){
    m_z[(*riter).second]=cl;
    nn++;
    if(nn>=nperclass && cl<m_k){
      nn=0;
      cl++;
    }
  }
  for(int k=1;k<=m_k;k++){
    MDGamma mdg(m_d);
    vector<ColumnVector> dat;
    for(int i=1;i<=m_n;i++)
      if(m_z[i-1] == k){
	dat.push_back(m_data.Row(i).t());
	m_z[i-1] -- ;
      }
    mdg.postupdate(dat,m_mdg0);
    m_mdg.push_back(mdg);
    m_classnd.push_back((int)dat.size());
  }

}

void EXPDPM_GibbsSampler::add_class(const Matrix& data){
  MDGamma mdg(m_d);
  mdg.postupdate(data,m_mdg0);
  m_mdg.push_back(mdg);
}

void EXPDPM_GibbsSampler::delete_empty_classes(){
  for(int k=m_k-1;k>=0;k--){
    if(m_classnd[k] == 0){
      for(int i=0;i<m_n;i++)
	if(m_z[i]>k)m_z[i]--;
      for(int kk=k;kk<m_k-1;kk++){
	m_classnd[kk]=m_classnd[kk+1];
	m_mdg[kk]=m_mdg[kk+1];
      }
      m_classnd.pop_back();
      m_mdg.pop_back();
      m_k--;
    }
  }
}


void EXPDPM_GibbsSampler::sample_own_params(){


  // update classes posteriors
  vector< vector<ColumnVector> > data;
  data.resize(m_k);

  // calculate likelihood
  m_likelihood = 0;
  for(int i=0;i<m_n;i++){
    data[ m_z[i] ].push_back(m_data.Row(i+1).t());
    m_likelihood += -marglik(m_data.Row(i+1).t(),m_z[i]);
  }

  for(int k=0;k<m_k;k++){
    if(data[k].size()>0)
      m_mdg[k].postupdate(data[k],m_mdg0);
  }

}


void EXPDPM_GibbsSampler::sample_own_hypers(){
  // sample b in m_mdg0
  ColumnVector lambda(m_d);
  lambda = 0.0;
  for(int k=0;k<m_k;k++)
    lambda += m_mdg[k].get_smdg();
  lambda = lambda.Sum();

  MDGamma mdg(float(m_k*m_d)*m_mdg0.get_a(),lambda);

  m_mdg0.set_b(mdg.get_smdg());
  m_mdg0.sample();

}

double EXPDPM_GibbsSampler::marglik(const ColumnVector& data,const int k){
  double res=0.0;

  for(int i=1;i<=m_d;i++){
    res += log(m_mdg[k].get_smdg(i)) - m_mdg[k].get_smdg(i)*data(i);
  }


  return res-m_maxloglik;
}
double EXPDPM_GibbsSampler::marglik_debug(const ColumnVector& data,const int k){
  double res=0.0;

  for(int i=1;i<=m_d;i++){
    res += log(m_mdg[k].get_smdg(i)) - m_mdg[k].get_smdg(i)*data(i);
  }


  return res-m_maxloglik;
}
double EXPDPM_GibbsSampler::margint(const ColumnVector& data){
  //double res=m_margintbase;

  //for(int i=1;i<=m_d;i++)
  //res += m_mdg0.get_a(i)*log(m_mdg0.get_b(i)) -
  //  (m_mdg0.get_a(i)+1) * log(m_mdg0.get_b(i)+data(i));

  //  OUT(m_maxloglik);
  //  OUT(res-m_maxloglik);


  // sample from the prior distribution

  double res=0.0;
  MDGamma mdg(m_mdg0.get_a(),m_mdg0.get_b());

  for(int i=1;i<=m_d;i++){
    res += log(mdg.get_smdg(i)) - mdg.get_smdg(i)*data(i);
  }

  return res-m_maxloglik;
}

}
