#if !defined(_DPM_UTILS_H)
#define _DPM_UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <string>
#include <algorithm>

#include "newran/newran.h"
#include "miscmaths/miscprob.h"
#include "armawrap/newmatio.h"

#include "gibbs.h"


namespace DPM {

// Wishart random generator
NEWMAT::ReturnMatrix wishrnd(const NEWMAT::SymmetricMatrix&,const int);
NEWMAT::ReturnMatrix iwishrnd(const NEWMAT::SymmetricMatrix&,const int);
float gamrnd(const float&,const float&);
float gampdf(const float&,const float&,const float&);

float distfisher(const float&,const float&,const float&,const float&);

// Gaussian-InverWishart distribution
// p(mu,sigma)=det(sigma)^(-(nu+d)/2-1)exp(-trace(Nu*inv(sigma))/2 -kappa/2*(mu-m_mu)'inv(sigma)(mu-m_mu))
class GaussianWishart{
 private:
  friend std::ostream& operator << (std::ostream& o,GaussianWishart& g);
 protected:
  NEWMAT::ColumnVector       m_mu;
  NEWMAT::SymmetricMatrix    m_Nu;
  float              m_kappa;
  int                m_dof;
  int                m_dim;

  NEWMAT::ColumnVector       m_smu;     // sample mean
  NEWMAT::SymmetricMatrix    m_ssigma;  // sample covariance


 public:
  GaussianWishart(){}
  GaussianWishart(const int dim):m_dim(dim){
    m_mu.ReSize(m_dim);
    m_Nu.ReSize(m_dim);
  }
  GaussianWishart(const NEWMAT::ColumnVector& mu,const NEWMAT::SymmetricMatrix& Nu,const int dof,const float& kappa):
    m_mu(mu),m_Nu(Nu),m_kappa(kappa),m_dof(dof){
    m_dim=m_mu.Nrows();
    sample();
  }
  ~GaussianWishart(){}
  inline NEWMAT::ColumnVector get_mu()const{return m_mu;}
  void set_mu(const NEWMAT::ColumnVector& mu){m_mu=mu;}
  inline NEWMAT::SymmetricMatrix get_Nu()const{return m_Nu;}
  void set_Nu(const NEWMAT::SymmetricMatrix& Nu){m_Nu=Nu;}
  void set_kappa(const float& kappa){m_kappa=kappa;}
  inline float get_kappa()const{return m_kappa;}
  inline int get_dof()const{return m_dof;}

  void postupdate(const std::vector<NEWMAT::ColumnVector>& data,const GaussianWishart& gw0){
    NEWMAT::ColumnVector mdat(m_dim);
    NEWMAT::SymmetricMatrix S(m_dim),SS(m_dim);

    float n = (float)data.size();
    m_dof   = gw0.get_dof()   + int(n);
    m_kappa = gw0.get_kappa() + n;
    mdat=0;S=0,SS=0;
    for(int i=0;i<int(n);i++){
      SS << data[i]*data[i].t();
      S  += SS;
      mdat += data[i];
    }
    mdat /= n;

    SS << S - n*mdat*mdat.t();
    SS << SS + gw0.get_kappa()*n/m_kappa * (mdat-gw0.get_mu())*(mdat-gw0.get_mu()).t();

    m_mu    = ( gw0.get_kappa()*gw0.get_mu() + n*mdat )/m_kappa;
    m_Nu   << gw0.get_Nu() + SS;

    sample();
  }
  void postupdate(const NEWMAT::Matrix& data,const GaussianWishart& gw0){
    NEWMAT::ColumnVector mdat(m_dim);
    NEWMAT::SymmetricMatrix S(m_dim),SS(m_dim);

    float n = (float)data.Nrows();
    m_dof   = gw0.get_dof()   + int(n);
    m_kappa = gw0.get_kappa() + n;
    mdat=0;S=0,SS=0;
    for(int i=1;i<=int(n);i++){
      SS << data.Row(i).t()*data.Row(i);
      S  += SS;
      mdat += data.Row(i).t();
    }
    mdat /= n;

    SS << S -n*mdat*mdat.t();
    SS << SS + gw0.get_kappa()*n/m_kappa * (mdat-gw0.get_mu())*(mdat-gw0.get_mu()).t();

    m_mu    = ( gw0.get_kappa()*gw0.get_mu() + n*mdat )/m_kappa;
    m_Nu   << gw0.get_Nu() + SS;

    sample();
  }
  void sample(NEWMAT::ColumnVector& mu,NEWMAT::SymmetricMatrix& sigma){
    sigma = DPM::iwishrnd(m_Nu.i(),m_dof);
    mu    = mvnrnd(m_mu.t(),sigma/m_kappa).t();
  }
  void sample(){
    m_ssigma = DPM::iwishrnd(m_Nu.i(),m_dof);
    m_smu    = mvnrnd(m_mu.t(),m_ssigma/m_kappa).t();
  }
  void postmode(const std::vector<NEWMAT::ColumnVector>& data,const GaussianWishart& gw0){
    NEWMAT::ColumnVector mdat(m_dim);
    NEWMAT::SymmetricMatrix S(m_dim),SS(m_dim);

    float n = (float)data.size();
    m_dof   = gw0.get_dof()   + int(n);
    m_kappa = gw0.get_kappa() + n;
    mdat=0;S=0,SS=0;
    for(int i=0;i<int(n);i++){
      SS << data[i]*data[i].t();
      S  += SS;
      mdat += data[i];
    }
    mdat /= n;

    SS << S -n*mdat*mdat.t();
    SS << SS + gw0.get_kappa()*n/m_kappa * (mdat-gw0.get_mu())*(mdat-gw0.get_mu()).t();

    m_mu    = ( gw0.get_kappa()*gw0.get_mu() + n*mdat )/m_kappa;
    m_Nu   << gw0.get_Nu() + SS;

    //mode();
    sample();

  }
  void mode(){
    m_ssigma = 1/float(m_dof-m_dim-1) * m_Nu;//iwishrnd(m_Nu.i(),m_dof);
    m_smu    = m_mu;//mvnrnd(m_mu.t(),m_ssigma/m_kappa).t();
  }
  void print(std::ostream& os)const{
    os << "Gaussian-InverseWishart distribution" << std::endl;
    os << "mean       : " << m_mu.t();
    os << "variance   : " << m_Nu.Row(1);
    for(int i=2;i<=m_dim;i++)
      os << "             "<<m_Nu.Row(i);
    os << "dof        : "<<m_dof<<std::endl;
    os << "kappa      : "<<m_kappa<<std::endl;
    os << "sample mu  : "<<m_smu.t();
    os << "sample var : "<<m_ssigma.Row(1);
    for(int i=2;i<=m_dim;i++)
      os << "             "<<m_ssigma.Row(i);
    os << "-----------------------------------"<<std::endl;
  }
  NEWMAT::ColumnVector get_smu()const{return m_smu;}
  void set_smu(const NEWMAT::ColumnVector& smu){m_smu=smu;}
  NEWMAT::SymmetricMatrix get_ssigma()const{return m_ssigma;}
  void set_ssigma(const NEWMAT::SymmetricMatrix& ss){m_ssigma=ss;}
  GaussianWishart& operator=(const GaussianWishart& rhs){
    m_mu     = rhs.m_mu;
    m_Nu     = rhs.m_Nu;
    m_kappa  = rhs.m_kappa;
    m_dof    = rhs.m_dof;
    m_dim    = rhs.m_dim;

    m_smu    = rhs.m_smu;
    m_ssigma = rhs.m_ssigma;

    return *this;
  }

};
// Multidimensional gamma distribution
class MDGamma{
 private:
  friend std::ostream& operator << (std::ostream& o,MDGamma& g);
 protected:
  NEWMAT::ColumnVector m_a;
  NEWMAT::ColumnVector m_b;

  int                  m_dim;

  NEWMAT::ColumnVector m_smdg; // sample from this distribution

 public:
  MDGamma(){}
  MDGamma(const int dim):m_dim(dim){
    m_a.ReSize(m_dim);
    m_b.ReSize(m_dim);
    m_smdg.ReSize(m_dim);
  }
 MDGamma(const int dim,const float a,const float b):m_dim(dim){
    m_a.ReSize(m_dim);
    m_b.ReSize(m_dim);
    m_smdg.ReSize(m_dim);
    m_a = a;m_b = b;
    sample();
  }
  MDGamma(const NEWMAT::ColumnVector& a,const NEWMAT::ColumnVector& b):m_a(a),m_b(b){
    m_dim=m_a.Nrows();
    m_smdg.ReSize(m_dim);
    sample();
  }
  ~MDGamma(){}
  inline NEWMAT::ColumnVector get_a()const{return m_a;}
  inline float get_a(const int i)const{return m_a(i);}
  void set_a(const NEWMAT::ColumnVector& a){m_a=a;}
  inline NEWMAT::ColumnVector get_b()const{return m_b;}
  inline float get_b(const int i)const{return m_b(i);}
  void set_b(const NEWMAT::ColumnVector& b){m_b=b;}

  void postupdate(const std::vector<NEWMAT::ColumnVector>& data,const MDGamma& mdg){
    int n=(int)data.size();

    NEWMAT::ColumnVector m(m_dim);
    m=0;
    for(unsigned int i=0;i<data.size();i++)
      m+=data[i];

    m_a = mdg.get_a() * float(n);
    m_b = mdg.get_b() + m;

    sample();

  }
  void postupdate(const NEWMAT::Matrix& data,const MDGamma& mdg){
    int n=data.Nrows();
    NEWMAT::ColumnVector m(m_dim);
    m=0;
    for(int i=1;i<=n;i++)
      m+=data.Row(i).t();

    m_a = mdg.get_a() * float(n*m_dim);
    m_b = mdg.get_b() + m;

    sample();
  }
  void sample(NEWMAT::ColumnVector& g){
    g.ReSize(m_dim);
    for(int i=1;i<=m_dim;i++)
      g(i) = gamrnd(m_a(i),m_b(i));
  }
  void sample(){
    for(int i=1;i<=m_dim;i++)
      m_smdg(i) = gamrnd(m_a(i),m_b(i));
  }
  void print(std::ostream& os)const{
    os << "Multidimensional Gamma distribution" << std::endl;
    os << "shape         : " << m_a.t();
    os << "inverse-scale : " << m_b.t();
    os << "sample        : " << m_smdg.t();
    os << "-----------------------------------"<<std::endl;
  }
  NEWMAT::ColumnVector get_smdg()const{return m_smdg;}
  float get_smdg(const int i)const{return m_smdg(i);}
  MDGamma& operator=(const MDGamma& rhs){
    m_a      = rhs.m_a;
    m_b      = rhs.m_b;
    m_dim    = rhs.m_dim;

    m_smdg   = rhs.m_smdg;

    return *this;
  }

};


bool compare(const std::pair<float,int> &r1,const std::pair<float,int> &r2);
void randomise(std::vector< std::pair<float,int> >& r);
std::vector< std::pair<float,int> > randomise(const int n);
float do_kmeans(const NEWMAT::Matrix&,std::vector<int>&,int&);
 void do_kmeans(const NEWMAT::Matrix&,std::vector<int>&,int&,const int&);


}

#endif
